var fs   = require('fs');
var path = require('path');
//var co   = require('co');

exports.onload = function(app) {
    global.entity = {};
    var routes = path.join(__dirname,'../','entity/');
    module.exports.AnalyticalRouting(routes,app);
};

/**
 * 动态加载路由
 * @param routes
 * @param app
 * @constructor
 */
module.exports.AnalyticalRouting = function (routes,app) {
    var routesFile = fs.readdirSync(routes);
    routesFile.forEach(function (files) {
        var filePath = path.normalize(routes + files);
        var stat = fs.statSync(filePath);
        if (stat.isFile()) {
            filePath = filePath.replace('.js', '');
            var routing = filePath.replace(path.join(__dirname, '../', 'entity/'), '').replace(/\\/g,'/');;
            if (filePath.indexOf('//') >= 0)
                routing = routing.replace(/\\/g, '_');
            else
                routing = routing.replace(/\//g, '_');
            entity[routing] = require(filePath);
        }
        if (stat.isDirectory())
            module.exports.AnalyticalRouting(filePath + "/", app);
    });
};