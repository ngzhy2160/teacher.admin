var log4js = require('log4js');

exports.onload = function(app,path) {
    log4js.configure({
        appenders: [
            {type: 'console'},
            {
                type: 'file', //文件输出
                filename: path + 'logs/access.log',
                maxLogSize: 1024,
                backups: 3,
                 //category: 'console'
            }
        ],
        replaceConsole: true
    });
    var logger = log4js.getLogger('console');
    logger.setLevel('INFO');
    app.use(log4js.connectLogger(logger, {level: log4js.levels.INFO, format: ':method :url'}));
};