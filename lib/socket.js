var http = require('http');
var socketio = require('socket.io');

exports.onload = function () {
    var config = require('../../config');
    var server = http.createServer(function () {});
    server.listen(config.socket.port);
    var io = socketio.listen(server,{'transports': ['websocket', 'polling']});

    /**
     * socket连接成功
     */
    io.sockets.on("connection", function (socket) {

        socket.on("message",function(obj) {
            io.emit('SendMessage',obj);
        })
    });
};