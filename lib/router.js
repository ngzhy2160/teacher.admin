var fs   = require('fs');
var path = require('path');

exports.onload = function(app) {
    var routes = path.join(__dirname, '../', 'routes/');
    module.exports.AnalyticalRouting(routes, app);
};

/**
 * 动态加载路由
 * @param routes
 * @param app
 * @constructor
 */
module.exports.AnalyticalRouting = function (routes,app) {
    var routesFile = fs.readdirSync(routes);
    routesFile.forEach(function (files) {
        var filePath = path.normalize(routes + files);
        var stat = fs.statSync(filePath);

        if (stat.isFile()) {
            filePath = filePath.replace('.js', '');
            var routing = filePath.replace(path.join(__dirname, '../', 'routes'), '').replace(/\\/g,'/');;
            if (routing.indexOf('//') >= 0)
                routing = routing.replace(/\\/g, '/');
            if (routing.indexOf('index') >= 0)
                app.use(routing.replace('index', ''), require(filePath));
            app.use(routing, require(filePath));
        }
        if (stat.isDirectory())
            module.exports.AnalyticalRouting(filePath + "/", app);
    });
};

