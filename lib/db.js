var mysql = require("mysql");
var pool = mysql.createPool({
    host: helper.config.DB.Address,
    user: helper.config.DB.UserName,
    password: helper.config.DB.PassWord,
    insecureAuth: true,
    database:helper.config.DB.DataBase
});

helper.mysql.query = function (table,sql,callback) {
    pool.getConnection(function (err, connection) {
        if (err) callback(err, null);
        if (!table) connection.query("use " + helper.config.DB.DataBase);
        else connection.query("use " + table);
        connection.query(sql, function (errs, value) {
            connection.release();
            callback(errs, value);
        })
    })
};
helper.mysql.insert = function (table,sql,params,callback) {
    pool.getConnection(function (err, connection) {
        if (err) callback(err, null);
        if (!table) connection.query("use " + helper.config.DB.DataBase);
        else connection.query("use " + table);
        connection.query(sql,params ,function (errs, value) {
            connection.release();
            callback(errs, value);
        })
    })
};
