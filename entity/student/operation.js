/**
 * 删除学生
 * @param list
 * @param callbaclk
 * @constructor
 */
module.exports.DelStudent = function(teacher_id,grade_id,list,callback) {
    if (list && list.length > 0) {
        helper.co(function* () {
            var delete_student_sql = "insert into tb_teacher_student(`teacher_id`,`grade_id`,`class_num`,`student_id`,`user_id`,`create_time`) values";
            for(var i=0;i<list.length;i++) {
                if (i == 0)
                    delete_student_sql += "('" + teacher_id + "','" + grade_id + "', '" + list[i].class_num + "', '" + list[i].student_id + "', '" + list[i].user_id + "','" + helper.moment(new Date()).format("YYYY-MM-DD HH:mm:ss") + "' )";
                else
                    delete_student_sql += ",('" + teacher_id + "','" + grade_id + "', '" +list[i].class_num + "', '" + list[i].student_id + "', '" + list[i].user_id + "','" + helper.moment(new Date()).format("YYYY-MM-DD HH:mm:ss") + "' )";
            }
            var delete_student = yield function (callback) {
                helper.mysql.query("", delete_student_sql, callback);
            };
            if (delete_student && delete_student.affectedRows > 0) return {err: 0};
            else return {err: 1};
        }).then(function (value) {
            callback(null,value);
        }).catch(function (err) {
            console.error(err);
            callback({err: 4},null);
        })
    } else {
        callback({err: 1});
    }
};

module.exports.SaveStudent = function (teacher_id,grade_id,class_num,student_id,user_id,callback) {
    var insert_student_sql = "delete from tb_teacher_student where teacher_id='" + teacher_id + "' and grade_id='" + grade_id + "' and class_num='" + class_num + "' and student_id='" + student_id + "'";
    helper.mysql.query("", insert_student_sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc && doc.affectedRows > 0) callback(null, {err: 0});
        else callback(null, {err: 1});
    })
};

module.exports.QueryTeacherStudent = function (teacher_id,grade_id,class_num,callback) {
    var sql = "select users.user_id,Tstudent.student_id,Tstudent.grade_id,Tstudent.class_num,users.username,student.student_name from tb_teacher_student as Tstudent left join tb_exam_student as student on student.student_id=Tstudent.student_id " +
        "left join tb_sysmgr_user as users on users.user_id=Tstudent.user_id where Tstudent.teacher_id='" + teacher_id + "' and Tstudent.grade_id='" + grade_id + "' and Tstudent.class_num='" + class_num + "' group by Tstudent.student_id";
    helper.mysql.query("", sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc) callback(null, doc);
        else callback(null, null);
    })
};