/**
 * 根据教师ID查询班级年级
 * @param teacher_id
 * @param callback
 * @constructor
 */
module.exports.QueryClassesByTeacherId = function (teacher_id,callback) {
    var sql = "select classes.teacher_id,classes.grade_id,classes.class_num,grade.grade_name from tb_teacher_classes as classes left JOIN tb_exam_grade as grade " +
        "on classes.grade_id =  grade.grade_id where classes.teacher_id = '" + teacher_id + "'";
    helper.mysql.query("", sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc) callback(null, doc);
        else callback(null, null);
    })
};

module.exports.QueryTeacher = function (teacher_id,callback) {
    var sql = "select * from tb_teacher_user where teacher_id='"+teacher_id+"'";
    helper.mysql.query("", sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc) callback(null, doc);
        else callback(null, null);
    })
};

module.exports.QueryClassStudent = function (teacher_id,school_id,school_name,grade_id,class_num,page,callback) {
    var pageSize = 16;
    var limit = (page - 1) * pageSize;

    helper.co(function* () {
        var result = {};
        result.page = page;
        result.total_page = 0;
        result.totalcount = 0;
        var Find_del_student_sql = "select student_id from tb_teacher_student where teacher_id='" + teacher_id + "' and grade_id='" + grade_id + "' and class_num='" + class_num + "'";


        var find_del_student = yield function (callback) {
            helper.mysql.query("", Find_del_student_sql, callback);
        };
        var del_student_list;
        if (find_del_student && find_del_student.length > 0) {
            for (var i = 0; i < find_del_student.length; i++) {
                if (find_del_student.length == 1) {
                    del_student_list = "(" + "'" + find_del_student[i].student_id + "')";
                } else {
                    if (i == 0)
                        del_student_list = "(" + "'" + find_del_student[i].student_id + "'";
                    else if (i == find_del_student.length - 1)
                        del_student_list += ",'" + find_del_student[i].student_id + "')";
                    else
                        del_student_list += ",'" + find_del_student[i].student_id + "'";
                }
            }
        }
        var student_count_sql = "select count(*) as num from tb_exam_student as student where";
        if (school_id && school_id != "")
            student_count_sql += " student.school_id='" + school_id + "'";
        else
            student_count_sql += " student.school_name='" + school_name + "'";
        if (del_student_list)
            student_count_sql += " and student.student_id not in " + del_student_list;
        student_count_sql += " and student.grade_id='" + grade_id + "' and student.class_num='" + class_num + "' and student.class_num is NOT NULL and student.class_num <> ''";

        var student_sql = "select student.student_id,student.student_name,users.username,users.user_id from tb_exam_student as student left join tb_sysmgr_user as users on users.user_id=student.user_id " +
            " where ";
        if (school_id && school_id != "")
            student_sql += "student.school_id='" + school_id + "'";
        else
            student_sql += "student.school_name='" + school_name + "'";
        if (del_student_list)
            student_sql += " and student.student_id not in " + del_student_list;
        student_sql += " and student.grade_id='" + grade_id + "' and student.class_num='" + class_num + "' and student.class_num is NOT NULL and student.class_num <> '' order by student.student_name,users.username limit " + limit + "," + pageSize;
        //查询总条数
        var total_count = yield function (callback) {
            helper.mysql.query('', student_count_sql, callback);
        };
        if (total_count && total_count.length > 0) {
            result.totalcount = total_count[0].num;
            result.total_page = Math.ceil(total_count[0].num / pageSize);
            var list = yield function (callback) {
                helper.mysql.query('', student_sql, callback)
            };
            result.list = list;
        }
        result.pageSize = pageSize;
        return result;

    }).then(function (value) {
        callback(null, {err: 0, result: value});
    }).catch(function (err) {
        console.error(err);
        callback({err: 1}, null);
    });
};

module.exports.QueryClassesByGradeId = function (teacher_id,grade_id,callback) {
    var sql = "select * from tb_teacher_classes where teacher_id='" + teacher_id + "' and grade_id='" + grade_id + "'";
    helper.mysql.query("", sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc) callback(null, doc);
        else callback(null, null);
    })
};

module.exports.QueryClassesByTeacherIdGradeId = function (teacher_id,grade_id,class_num,callback) {
    var sql = "select classes.teacher_id,classes.grade_id,classes.class_num,grade.grade_name from tb_teacher_classes as classes left JOIN tb_exam_grade as grade " +
        "on classes.grade_id =  grade.grade_id where classes.teacher_id = '" + teacher_id + "' and grade.grade_id='"+grade_id+"' and classes.class_num='"+class_num+"'";
    helper.mysql.query("", sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc) callback(null, doc);
        else callback(null, null);
    })
};

/**
 * 获取个人中心 知识点进度选项
 * @param userid
 * @param callback
 * @constructor
 */
module.exports.QueryExamPointsByUserid=function(userid,callback) {
    var sql = "";
    sql += " SELECT  DISTINCT tb_cms_knowledge_point.point_id,point_name";
    sql += " FROM tb_cms_knowledge_point RIGHT JOIN tb_exam_record";
    sql += " on tb_cms_knowledge_point.point_id=tb_exam_record.point_id WHERE   tb_exam_record.user_id='" + userid + "'   and tb_exam_record.point_id  is not null  and tb_exam_record.point_id !='undefined'";
    sql += " ORDER BY point_name;";

    helper.mysql.query('', sql, function (err, doc) {
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null, JSON.stringify({err: 0, result: doc}));
    })
};

/**
 * 根据用户id查询用户信息和用户扩展信息
 * @param user_id        用户id
 * @param callback
 * @constructor
 */
module.exports.QueryUserInfoByUserId = function (user_id,callback) {
    var Find_student = helper.util.format("select * from tb_exam_student where user_id='%s'",user_id);
    helper.mysql.query('',Find_student, function (err_student,doc_sutendt) {
        if(err_student) callback({err:1},null);
        if(doc_sutendt && doc_sutendt[0] && doc_sutendt[0].school_id){
            var sql = "SELECT " +
                "users.user_id,users.love,users.username,users.fullname,users.mobile, users.email, student.school_id,student.portrait,student.constellation,student.bloodtype,student.sex,student.birthday," +
                "student.student_name,student.hometown_province,student.domicile_type,student.domicile,student.hometown_city,student.school_type, student.student_id, student.province_id, student.city_id,student.grade_id," +
                " student.area_id, student.mater_id,student.math_anxiety_score, school.school_name, student.subject_id, student.class_num,student.hobby,student.books,student.idol,student.motto,student.wish,student.self_introduction " +
                "FROM tb_sysmgr_user AS users LEFT JOIN tb_exam_student AS student ON users.user_id = student.user_id " +
                " left join tb_exam_school as school on school.school_id = student.school_id " +
                "WHERE users.enabled = 1 AND users.locked = 0 AND users.user_id = '" + user_id + "'";

            helper.mysql.query('', sql, function (err, doc) {
                if (err) callback({err: 1}, null);
                if (doc) callback(null, {err: 0, result: doc});
            })
        }else{
            var sql = "SELECT " +
                "users.user_id,users.love,users.username,users.fullname,users.mobile, users.email,student.school_id, student.portrait,student.constellation,student.bloodtype,student.sex,student.birthday," +
                "student.student_name,student.hometown_province,student.domicile_type,student.domicile,student.hometown_city,student.school_type, student.student_id, student.province_id, student.city_id,student.grade_id," +
                " student.area_id, student.mater_id,student.math_anxiety_score, student.school_name, student.subject_id, student.class_num,student.hobby,student.books,student.idol,student.motto,student.wish,student.self_introduction " +
                "FROM tb_sysmgr_user AS users LEFT JOIN tb_exam_student AS student ON users.user_id = student.user_id " +
                "WHERE users.enabled = 1 AND users.locked = 0 AND users.user_id = '" + user_id + "'";

            helper.mysql.query('', sql, function (err, doc) {
                if (err) callback({err: 1}, null);
                if (doc) callback(null, {err: 0, result: doc});
            })
        }
    })

};

/**
 * 根据学科id,教材id,年级id查询章节知识点
 * @param subject_id    学科id
 * @param material_id   教材id
 * @param grade_id      年级id
 * @param callback
 * @constructor
 */
module.exports.QueryChap = function (subject_id,material_id,grade_id,callback) {
    var sql = "select kp.point_id,kp.point_name from tb_cms_klpoint_chapter_rel as kcr " +
        "left join tb_cms_material_grade as mg on kcr.teach_rel_id = mg.id " +
        "left join tb_cms_knowledge_point as kp on kp.point_id = kcr.chap_id " +
        "where mg.material_id='" + material_id + "' " +
        "and mg.grade_id='" + grade_id + "' " +
        "and mg.subject_id='" + subject_id + "' " +
        "group by kp.point_id order by kp.sort_order;";
    helper.mysql.query('',sql, function (err,doc) {
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
    })
};

/**
 * 获取个人中心单项知识点
 * @param userid
 * @param pointid
 * @param callback
 * @constructor
 */
module.exports.QueryExamPointsDateTitle=function(userid,pointid,callback) {
    var sql="";
    if(pointid){
        sql="";
        sql+=" SELECT count(point_id) as num,createtime,formatdate FROM (";
        sql+=" SELECT tb_cms_knowledge_point.point_id,point_name,date_format(tb_exam_record.create_time,'%Y-%m-%d')   as createtime ,date_format(tb_exam_record.create_time,'%m-%d')   as formatdate";
        sql+=" FROM tb_cms_knowledge_point RIGHT JOIN tb_exam_record";
        sql+=" on tb_cms_knowledge_point.point_id=tb_exam_record.point_id WHERE   tb_exam_record.user_id='"+userid+"'  and tb_exam_record.point_id='"+pointid+"'   GROUP BY createtime, tb_cms_knowledge_point.point_id ORDER BY createtime) as dt GROUP BY createtime ORDER BY createtime DESC  LIMIT 10";
    }
    else{
        sql="";
        sql+=" SELECT count(point_id) as num,createtime,formatdate FROM (";
        sql+=" SELECT tb_cms_knowledge_point.point_id,point_name,date_format(tb_exam_record.create_time,'%Y-%m-%d')   as createtime ,date_format(tb_exam_record.create_time,'%m-%d')   as formatdate";
        sql+=" FROM tb_cms_knowledge_point RIGHT JOIN tb_exam_record";
        sql+=" on tb_cms_knowledge_point.point_id=tb_exam_record.point_id WHERE   tb_exam_record.user_id='"+userid+"' GROUP BY createtime, tb_cms_knowledge_point.point_id ORDER BY createtime DESC) as dt GROUP BY createtime ORDER BY createtime DESC LIMIT 10";
    }

    helper.mysql.query('', sql, function (err, doc) {
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
    })
};

/**
 * 获取单项知识点做题明细
 * @param userid
 * @param pointid
 * @param callback
 * @constructor
 */
module.exports.QueryExamPointsItems=function(userid,pointid,callback) {
    var sql="";
    if(pointid){
        sql="";
        sql=" SELECT tb_cms_knowledge_point.point_id,point_name,tb_exam_record.answer,tb_exam_record.correct_answer  ,date_format(tb_exam_record.create_time,'%Y-%m-%d')   as createtime ,date_format(tb_exam_record.create_time,'%m-%d')   as formatdate FROM tb_cms_knowledge_point RIGHT JOIN tb_exam_record";
        sql+=" on tb_cms_knowledge_point.point_id=tb_exam_record.point_id WHERE   user_id='"+userid+"' and tb_exam_record.point_id='"+pointid+"' ORDER BY createtime DESC";
    }
    else{
        sql="SELECT tb_cms_knowledge_point.point_id,point_name,tb_exam_record.answer,tb_exam_record.correct_answer ,date_format(tb_exam_record.create_time,'%Y-%m-%d')   as createtime ,date_format(tb_exam_record.create_time,'%m-%d')   as formatdate FROM tb_cms_knowledge_point RIGHT JOIN tb_exam_record ";
        sql+=" on tb_cms_knowledge_point.point_id=tb_exam_record.point_id WHERE   tb_exam_record.user_id='"+userid+"'  ORDER BY createtime DESC;";
    }

    helper.mysql.query('', sql, function (err, doc) {
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
    })
};

/**
 * 获取会员年级、教材等扩展信息
 */
module.exports.QueryExamStudent=function(userid,callback) {
    var sql="SELECT user_id,grade_id,province_id,city_id,area_id,mater_id,subject_id from tb_exam_student where user_id = '" + userid + "'";
    helper.mysql.query('', sql, function (err, doc) {
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
    });
};

/**
 * 获取教材对应年级下所有知识点
 * @param subjectid 学科
 * @param materialid 教材
 * @param gradeid 年级
 * @param callback
 * @constructor
 */
module.exports.QueryMaterialPoints=function(subjectid,materialid,gradeid,callback) {
    var sql = " SELECT tckp.point_id,tckp.point_name,tcktr.parent_id,tckp.sort_order, tcktr.id  FROM";
    sql += " tb_cms_klpoint_teach_rel tcktr,";
    sql += " tb_cms_material_grade tcmg,";
    sql += " tb_cms_knowledge_point tckp";
    sql += " WHERE tcktr.teach_rel_id = tcmg.id";
    sql += " AND tcmg.grade_id = '" + gradeid + "'";
    sql += " AND tcmg.material_id='" + materialid + "'";
    sql += " AND tcmg.subject_id='" + subjectid + "'";
    sql += " AND tcktr.klpoint_id=tckp.point_id";
    sql += " AND tcmg.state=1 GROUP BY tcktr.klpoint_id ORDER BY parent_id,tckp.sort_order;";

    helper.mysql.query('', sql, function (err, doc) {
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null, JSON.stringify({err: 0, result: doc}));
    })
};

/**
 * 获取用户知识点准确率
 * @param userid
 * @param pointidarraystring
 * @param callback
 * @constructor
 */
module.exports.QueryExamanalysisQuescountByUser=function(userid,pointidarraystring,callback) {
    var sql =  "SELECT  true_ques_count,dup_answ_ques_count,klpoint_id  FROM tb_exam_analysis_quescount  where " ;
    sql+=" user_id='"+userid+"' and  klpoint_id in("+pointidarraystring+")";

    helper.mysql.query('', sql, function (err, doc) {
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
    })
}