/**
 * 查询全部学校类型
 * @param callback
 * @constructor
 */
module.exports.QuerySchoolType = function (callback) {
    var sql = "select * from tb_exam_school_type";
    helper.mysql.query('', sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc) callback(null, doc);
        else callback(null, null);
    })
};

/**
 * 根据区县ID查询学校
 * @param area_id
 * @param callback
 * @constructor
 */
module.exports.QuerySchoolByAreaId = function (area_id,callback) {
    var sql = "select * from tb_exam_school where district_id='" + area_id + "'";
    helper.mysql.query("", sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc) callback(null, doc);
        else callback(null, null);
    })
};

/**
 * 根据学校信息查询年级
 * @param school_name
 * @param school_id
 * @param callback
 * @constructor
 */
module.exports.QueryGradeBySchool = function (school_name,school_id,callback) {
    var sql = "select student.student_id,student.grade_id,grade.grade_name from tb_exam_student as student " +
        "left join tb_exam_grade as grade on grade.grade_id = student.grade_id where ";
    if (school_id && school_id != "")
        sql += "student.school_id='" + school_id + "'";
    else
        sql += "student.school_name='" + school_name + "'";
    sql += " and student.class_num is NOT NULL and student.class_num <> ''  GROUP BY grade.grade_id ORDER BY grade.sort_order";

    helper.mysql.query("", sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc) callback(null, doc);
        else callback(null, null);
    })
};

module.exports.QueryClassBySchool = function (grade_id,school_id,school_name,callback) {
    var sql = "select class_num from tb_exam_student where ";
    if (school_id && school_id != "")
        sql += "school_id='" + school_id + "'";
    else
        sql += "school_name='" + school_name + "'";
    sql += " and grade_id='" + grade_id + "' and class_num is NOT NULL and class_num <> '' GROUP BY class_num";
    helper.mysql.query("", sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc) callback(null, doc);
        else callback(null, null);
    })
};