/**
 * 查询全部省份
 * @param callback
 * @constructor
 */
module.exports.QueryProvince = function (callback) {
    var sql = "select * from tb_cms_province";
    helper.mysql.query("", sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc) callback(null, doc);
        else callback(null, null);
    })
};

/**
 * 根据省份ID查询城市
 * @param province_id   省份ID
 * @param callback
 * @constructor
 */
module.exports.QueryCityByProvinceId = function (province_id,callback) {
    var sql = helper.util.format("select * from tb_cms_city where father='%s'", province_id);
    helper.mysql.query("", sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc) callback(null, doc);
        else callback(null, null);
    })
};

/**
 * 根据城市ID查询区县
 * @param city_id   省市ID
 * @param callback
 * @constructor
 */
module.exports.QueryAreaByCityId = function(city_id,callback) {
    var sql = helper.util.format("select * from tb_cms_area where father='%s'", city_id);
    helper.mysql.query("", sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc) callback(null, doc);
        else callback(null, null);
    })
};