/**
 * 根据教师ID查询教师信息
 * @param teacher_id    教师ID
 * @param callback
 * @constructor
 */
module.exports.QueryTeacherUserInfoByTeacherId = function (teacher_id,callback) {
    var sql = helper.util.format("select users.teacher_id,users.account,users.nickname,users.fullname,users.sex,users.birthday,users.school_type,school_type.school_type_name,users.school_id," +
        "users.mobile,users.school_name,users.province_id,users.city_id,users.area_id,users.address,users.school_name," +
        "province.province,city.city,area.area from tb_teacher_user as users left join tb_cms_province as province on users.province_id= province.provinceID " +
        "left join tb_cms_city as city on users.city_id = city.cityID " +
        "left join tb_cms_area as area on users.area_id = area.areaID " +
        "left join tb_exam_school_type as school_type on users.school_type = school_type.school_type_id where users.teacher_id='%s'", teacher_id);
    helper.mysql.query("", sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc) callback(null, doc);
        else callback(null, null);
    })
};

/**
 * 根据教师ID查询教师班级信息
 * @param teacher_id    教师ID
 * @param callback
 * @constructor
 */
module.exports.QueryTeacherClassByTeachId = function (teacher_id,callback) {
    var sql = helper.util.format("select classes.teacher_id,grade.grade_id,classes.class_num,grade.grade_name from tb_teacher_classes as classes " +
        "left join tb_exam_grade as grade on classes.grade_id = grade.grade_id where classes.teacher_id='%s'", teacher_id);
    helper.mysql.query("", sql, function (err, doc) {
        if (err)callback(err, null);
        if (doc) callback(null, doc);
        else callback(null, null);
    })
};