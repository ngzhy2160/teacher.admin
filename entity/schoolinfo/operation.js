
module.exports.SaveTeacherInfo = function (teacher_id,province_id,city_id,area_id,address,school_id,school_name,school_type,list,callback) {
    console.log("list2",list);
    helper.co(function* () {
        var Update_Teacher_Sql = helper.util.format("update tb_teacher_user set province_id='%s',city_id='%s',area_id='%s',address='%s',school_id='%s',school_name='%s',school_type='%s'," +
            "update_time='%s' where teacher_id='%s'", province_id, city_id, area_id, address, school_id, school_name, school_type, helper.moment(new Date()).format("YYYY-MM-DD HH:mm:ss"), teacher_id);
        var update_teacher_result = yield function (callback) {
            helper.mysql.query("", Update_Teacher_Sql, callback);
        };
        if (update_teacher_result && update_teacher_result.affectedRows > 0) {
            if (list && list.length > 0) {
                var delete_classes_sql = helper.util.format("delete from tb_teacher_classes where teacher_id='%s'", teacher_id);
                var delete_classes_result = yield function (callback) {
                    helper.mysql.query("",delete_classes_sql,callback);
                };
                for (var i = 0; i < list.length; i++) {
                    if (list[i].grade_id && list[i].grade_id != '-1' && list[i].class_num) {
                        var Insert_classes_Sql = helper.util.format("insert into tb_teacher_classes(`teacher_id`,`grade_id`,`class_num`,`create_time`) values('%s','%s','%s','%s')",
                            teacher_id,list[i].grade_id,list[i].class_num,helper.moment(new Date()).format("YYYY-MM-DD HH:mm:ss"));
                        var insert_class_result = yield function(callback){
                            helper.mysql.query("",Insert_classes_Sql,callback);
                        }
                    }
                }
            }
            var UpdateAuthority = yield function (callback) {
                module.exports.UpdateAuthority(teacher_id,callback);
            };
            return {err: 0};
        } else
            return {err: 1};
    }).then(function (value) {
        callback(null, value);
    }).catch(function (err) {
        console.error(err);
        callback(err, null);
    })
};


module.exports.UpdateAuthority = function (teacher_id,callback) {
    var update_teacher_sql = helper.util.format("update tb_teacher_user set islocked=%s,adjust_time='%s' where teacher_id='%s'",
        0, helper.moment(new Date()).format('YYYY-MM-DD HH:mm:ss'), teacher_id);

    helper.mysql.query("", update_teacher_sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc && doc.affectedRows > 0)callback(null, {err: 1});
        else callback(null, null);
    })
};