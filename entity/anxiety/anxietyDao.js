var $sql = require('../../entitySql/anxiety/anxietysql');

/**
 * 获取教师基本信息
 * @param callback
 */
module.exports.getStudentAnxiety=function(school_id,grade_id,mater_id,subject_id,class_num,callback){
    var sql=helper.util.format($sql.selectAnxietyScore,school_id,grade_id ,mater_id,subject_id,class_num);
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });
};


//===================以下为共通辅助方法================================

/**
 * guid
 * @returns {*}
 */
function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + S4() + S4() + S4() + S4() + S4() + S4());
}

/**
 * 日期格式化
 * @param time
 * @returns {string}
 */
function formatTime(time) {
    var now = new Date(time);
    var year = now.getFullYear();
    var month = (now.getMonth() + 1 > 9) ? (now.getMonth() + 1) : ('0' + (now.getMonth() + 1));
    var date = (now.getDate() > 9) ? now.getDate() : ('0' + now.getDate());
    var hour = (now.getHours() > 9) ? now.getHours() : ('0' + now.getHours());
    var minute = (now.getMinutes() > 9) ? now.getMinutes() : ('0' + now.getMinutes());
    var second = (now.getSeconds() > 9) ? now.getSeconds() : ('0' + now.getSeconds());
    return year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
}
