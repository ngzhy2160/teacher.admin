var $sql = require('../../entitySql/smslog/smslogsql');

/**
 * 插入短信验证码记录
 * @param generalinfo
 * @param callback
 */
module.exports.insertsmscode=function(code,type,account,callback){
    var id=guidGenerator();
    var sql=helper.util.format($sql.insert,id,code,type,account, new Date().getTime());
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });
};

/**
 * 根据手机号获取验证码
 * @param account
 * @param callback
 */
module.exports.selectsmscode=function(type,account,callback){
    var sql=helper.util.format($sql.selectbyaccount,type,account);
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });
};

/**
 * 更新验证码状态
 * @param account
 * @param callback
 */
module.exports.updateIsuseStatus=function(id,callback){
    var sql=helper.util.format($sql.updateIsuseStatus,id);
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });
};


module.exports.SendSMS = function (mobile, message, callback) {
    var http = require("http");
    var querystring = require("querystring");

    var result = "";
    var post_data = querystring.stringify({
        cdkey: "6SDK-EMY-6688-JETRN",
        password: "964959",
        phone: mobile,
        message: message,
        seqid: new Date().getTime() + parseInt(Math.random() * (1000)) + 1,
        smspriority: 5
    });
    var options = {
        host: "sdk4rptws.eucp.b2m.cn",
        path: "/sdkproxy/sendsms.action",
        method: 'POST',
        port: 8080,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
            'Content-Length': post_data.length
        }
    };
    var reqHttp = http.request(options, function (resHttp) {
        resHttp.setEncoding('utf8');
        resHttp.on('data', function (chunk) {
            result += delHtmlTag(chunk).trim();
        });
        resHttp.on('end', function () {
            callback(null, result);
        })
    });

    reqHttp.on('error', function (e) {
        console.error('problem with request: ' + e.message);
    });
    reqHttp.write(post_data);
    reqHttp.end();
};

/**
 * 删除html标签
 * @param str
 * @returns {*|{dist}|void|string|XML|{by}}
 */
function delHtmlTag (str) {
    return str.replace(/<[^>]+>/g, "");
};

/**
 * 发送邮箱
 * @param callback
 * @constructor
 */
module.exports.SendEmail = function (email,message,title,callback) {
    var nodemailer = require("nodemailer");
    var transport = nodemailer.createTransport("SMTP", {
        host: "smtp.qiye.163.com",
        secureConnection: true,
        port: 994,
        auth: {
            user: 'shujia@huaat.net',
            pass: "Huaat263"
        }
    });

    transport.sendMail({
        from: "shujia@huaat.net",
        to: email,
        subject: title,
        generateTextFromHTML: true,
        html: message
    }, function (err, doc) {
        transport.close();
        if (err)
            callback(null, {err: 1});
        if (doc)
            callback(null, {err: 0, result: doc});
    });
};

//===================以下为共通辅助方法================================

/**
 * guid
 * @returns {*}
 */
function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + S4() + S4() + S4() + S4() + S4() + S4());
}

/**
 * 日期格式化
 * @param time
 * @returns {string}
 */
function formatTime(time) {
    var now = new Date(time);
    var year = now.getFullYear();
    var month = (now.getMonth() + 1 > 9) ? (now.getMonth() + 1) : ('0' + (now.getMonth() + 1));
    var date = (now.getDate() > 9) ? now.getDate() : ('0' + now.getDate());
    var hour = (now.getHours() > 9) ? now.getHours() : ('0' + now.getHours());
    var minute = (now.getMinutes() > 9) ? now.getMinutes() : ('0' + now.getMinutes());
    var second = (now.getSeconds() > 9) ? now.getSeconds() : ('0' + now.getSeconds());
    return year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
}
