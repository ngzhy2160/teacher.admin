var $sql = require('../../entitySql/teacher/teachersql');

/**
 * 注册
 * @param account
 * @param password
 * @param callback
 */
module.exports.register=function(account,fullname,password,schoolname,schooltype,callback){
    var id=guidGenerator();
    var sql=helper.util.format($sql.register,id,account,password,fullname,schoolname,schooltype,formatTime(new Date()),0);
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });
};

/**
 * 判断账号是否已注册
 * @param account
 * @param callback
 */
module.exports.checkaccountExist=function(account,callback){
    var sql=helper.util.format($sql.checkaccount,account);
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });
};

/**
 * 登录
 * @param account  登录账号
 * @param password 登录密码
 * @param callback 回调函数
 */
module.exports.login=function(account,password,callback){
    var sql=helper.util.format($sql.selectbyaccountandpwd,account,password);
    //console.log('loginsql',sql);
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });

};

//===================以下为共通辅助方法================================

/**
 * guid
 * @returns {*}
 */
function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + S4() + S4() + S4() + S4() + S4() + S4());
}

/**
 * 日期格式化
 * @param time
 * @returns {string}
 */
function formatTime(time) {
    var now = new Date(time);
    var year = now.getFullYear();
    var month = (now.getMonth() + 1 > 9) ? (now.getMonth() + 1) : ('0' + (now.getMonth() + 1));
    var date = (now.getDate() > 9) ? now.getDate() : ('0' + now.getDate());
    var hour = (now.getHours() > 9) ? now.getHours() : ('0' + now.getHours());
    var minute = (now.getMinutes() > 9) ? now.getMinutes() : ('0' + now.getMinutes());
    var second = (now.getSeconds() > 9) ? now.getSeconds() : ('0' + now.getSeconds());
    return year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
}
