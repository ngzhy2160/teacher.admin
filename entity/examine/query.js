/**
 * 分页查询
 * @param limit
 * @param pageSize
 * @param page
 * @param callback
 * @constructor
 */
module.exports.QueryTeacherUserList = function (limit,pageSize,page,callback) {
    var result = {};
    result.page = {};
    result.list = [];
    result.page.total_page = 0;
    result.page.CurrentPage = page;
    result.page.TotalNumber = 0;
    result.page.PageNumber = pageSize;

    //查询数据总量
    var Find_Gauge_Sql = "select count(*) as num from tb_teacher_user";
    //分页查询
    var sql = helper.util.format("select tuser.teacher_id,tuser.account,tuser.nickname,tuser.fullname,tuser.mobile,tuser.address," +
        "tuser.school_name as user_school_name,tuser.school_id,school.school_name,tuser.province_id,tuser.city_id,tuser.area_id," +
        "tuser.school_type,tuser.islocked,schoolType.school_type_name,province.province,city.city,area.area from tb_teacher_user as tuser " +
        "left join tb_exam_school as school on tuser.school_name = school.school_name or tuser.school_id = school.school_id " +
        "left join tb_exam_school_type as schoolType on schoolType.school_type_id = tuser.school_type " +
        "left join tb_cms_province as province on province.provinceID = tuser.province_id " +
        "left join tb_cms_city as city on city.cityID = tuser.city_id left join tb_cms_area as area on area.areaID = tuser.area_id where limit %s,%s", limit, pageSize);
    helper.mysql.query('', Find_Gauge_Sql, function (err, doc) {
        if (err) callback(err, null);
        if (doc && doc.length > 0) {
            result.page.TotalNumber = doc[0].num || 0;
            result.page.total_page = Math.ceil(doc[0].num / pageSize);
            helper.mysql.query('', sql, function (err, doc) {
                if (err) callback(err, null);
                if (doc && doc.length > 0) {
                    result.list = doc || [];
                    callback(null,result);
                } else callback(null, null);
            })
        } else callback(null, null);
    });
};
