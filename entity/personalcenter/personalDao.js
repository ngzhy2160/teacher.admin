var $sql = require('../../entitySql/teacher/teachersql');

/**
 * 获取教师基本信息
 * @param callback
 */
module.exports.getteacherinfoById=function(teacherid,callback){
    var sql=helper.util.format($sql.selectbyid,teacherid);
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });
};

/**
 * 教师端常规设置保存
 * @param generalinfo
 * @param callback
 */
module.exports.savegeneralsetting=function(generalinfo,callback){
    var sql=helper.util.format($sql.updategeneralsetting,generalinfo.account,generalinfo.nickname,generalinfo.fullname,generalinfo.sex,generalinfo.birthday, formatTime(new Date()),generalinfo.teacherid);
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });
};


/**
 * 判断原始密码是否正确
 * @param teacherid
 * @param password
 * @param callback
 */
module.exports.checkpassword=function(teacherid,password,callback){
    var sql=helper.util.format($sql.checkpassword,teacherid,password);
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });
};

/**
 * 判断手机号是否已绑定
 * @param teacherid
 * @param mobile
 * @param callback
 */
module.exports.checkBindMobile=function(mobile,callback){
    var sql=helper.util.format($sql.checkmobile,mobile);
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });
};

/**
 * 根据账号获取信息
 * @param account
 * @param callback
 */
module.exports.selectbyaccount=function(account,callback){
    var sql=helper.util.format($sql.selectbyaccount,account);
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });
};

/**
 * 更新安全隐私
 * @param teacherid
 * @param newpassword
 * @param mobile
 * @param callback
 */
module.exports.updatesecuritysetting=function(teacherid,newpassword,mobile,callback){
    var sql=helper.util.format($sql.updatesecuritysetting,newpassword,mobile, formatTime(new Date()),teacherid);
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });
};

/**
 * 更新手机号码
 * @param teacherid
 * @param mobile
 * @param callback
 */
module.exports.updateMobile=function(teacherid,mobile,callback){
    var sql=helper.util.format($sql.updateMobile,mobile, formatTime(new Date()),teacherid);
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });
};

/**
 * 更新密码
 * @param password
 * @param account
 * @param callback
 */
module.exports.updatepasswordbyaccount=function(password,account,callback){
    var sql=helper.util.format($sql.updatepasswordbyaccount,password,formatTime(new Date()),account);
    helper.mysql.query('',sql,function(err,doc){
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    });
};


//===================以下为共通辅助方法================================

/**
 * guid
 * @returns {*}
 */
function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + S4() + S4() + S4() + S4() + S4() + S4());
}

/**
 * 日期格式化
 * @param time
 * @returns {string}
 */
function formatTime(time) {
    var now = new Date(time);
    var year = now.getFullYear();
    var month = (now.getMonth() + 1 > 9) ? (now.getMonth() + 1) : ('0' + (now.getMonth() + 1));
    var date = (now.getDate() > 9) ? now.getDate() : ('0' + now.getDate());
    var hour = (now.getHours() > 9) ? now.getHours() : ('0' + now.getHours());
    var minute = (now.getMinutes() > 9) ? now.getMinutes() : ('0' + now.getMinutes());
    var second = (now.getSeconds() > 9) ? now.getSeconds() : ('0' + now.getSeconds());
    return year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
}
