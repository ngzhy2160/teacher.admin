/**
 * 根据学校年级获取教材
 * @param schoolid
 * @param grade_id
 * @param callback
 * @constructor
 */
module.exports.QueryMaterInfo = function (schoolid,grade_id,callback) {
    var sql = "select mater_id,subject_id,grade_id,count(*) as count from tb_exam_student " +
        " where  school_id='" + schoolid + "' " +
        " and grade_id='" + grade_id + "' " +
        " group by school_id,grade_id having count>0;";
    helper.mysql.query('',sql, function (err,doc) {
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
    })
};

/**
 * 根据学科id,教材id,年级id查询章知识点
 * @param subject_id    学科id
 * @param material_id   教材id
 * @param grade_id      年级id
 * @param callback
 * @constructor
 */
module.exports.QueryChap = function (subject_id,material_id,grade_id,callback) {
    var sql = "select kp.point_id,kp.point_name from tb_cms_klpoint_chapter_rel as kcr " +
        "left join tb_cms_material_grade as mg on kcr.teach_rel_id = mg.id " +
        "left join tb_cms_knowledge_point as kp on kp.point_id = kcr.chap_id " +
        "where mg.material_id='" + material_id + "' " +
        "and mg.grade_id='" + grade_id + "' " +
        "and mg.subject_id='" + subject_id + "' " +
        "group by kp.point_id order by kp.sort_order;";
    helper.mysql.query('',sql, function (err,doc) {
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
    })
};

/**
 * 获取教材对应年级下所有知识点
 * @param subjectid 学科
 * @param materialid 教材
 * @param gradeid 年级
 * @param callback
 * @constructor
 */
module.exports.QueryMaterialPoints=function(subjectid,materialid,gradeid,callback) {
    var sql = " SELECT tckp.point_id,tckp.point_name,tcktr.parent_id,tckp.sort_order, tcktr.id  FROM";
    sql += " tb_cms_klpoint_teach_rel tcktr,";
    sql += " tb_cms_material_grade tcmg,";
    sql += " tb_cms_knowledge_point tckp";
    sql += " WHERE tcktr.teach_rel_id = tcmg.id";
    sql += " AND tcmg.grade_id = '" + gradeid + "'";
    sql += " AND tcmg.material_id='" + materialid + "'";
    sql += " AND tcmg.subject_id='" + subjectid + "'";
    sql += " AND tcktr.klpoint_id=tckp.point_id";
    sql += " AND tcmg.state=1 GROUP BY tcktr.klpoint_id ORDER BY parent_id,tckp.sort_order;";

    helper.mysql.query('', sql, function (err, doc) {
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null, JSON.stringify({err: 0, result: doc}));
        else callback(JSON.stringify({err: 1}), null);
    })
}

/**
 * 获取指定年级班级下学生信息
 * @param schoolid 学校
 * @param grade_id 年级
 * @param mater_id 教材
 * @param subject_id 学科
 * @param classnum 班级
 * @param callback
 * @constructor
 */
module.exports.GetAllStudents = function (schoolid,grade_id,mater_id,subject_id,classnum,callback) {
    var sql = "select * from tb_exam_student " +
        " where  school_id='" + schoolid + "' " +
        " and grade_id='" + grade_id + "' " +
        " and mater_id='" + mater_id + "' " +
        " and subject_id='" + subject_id + "' "+
        " and class_num='" + classnum + "' ";
    //console.log('sqll',sql)
    helper.mysql.query('',sql, function (err,doc) {
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
    })
};

/**
 * 获取班级知识点准确率
 * @param useridliststring
 * @param pointid
 * @param callback
 * @constructor
 */
module.exports.GetClassRate = function (useridliststring,pointid,callback) {
    var sql = "SELECT  sum(true_ques_count)  / sum(dup_answ_ques_count) *100 as pointrate FROM tb_exam_analysis_quescount " +
        " where user_id in(" + useridliststring + ") " +
        " and  klpoint_id ='" + pointid + "' ";

    helper.mysql.query('',sql, function (err,doc) {
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null,JSON.stringify({err: 0, result: doc}));
    })
};

/**
 * 获取用户知识点准确率
 * @param userid
 * @param pointid
 * @param callback
 * @constructor
 */
module.exports.GetUserRate = function (userid,pointid,callback) {
    var sql = " SELECT pointrate,student_name,tb_exam_analysis_quescount.user_id FROM"+
    " (SELECT  sum(true_ques_count)  / sum(dup_answ_ques_count) *100 as pointrate,user_id  FROM tb_exam_analysis_quescount" +
    " where  user_id ='" + userid + "'  and  klpoint_id ='" + pointid + "' )  as tb_exam_analysis_quescount" +
    " LEFT JOIN" +
    " (SELECT  CASE  WHEN   student_name is null THEN (SELECT username FROM tb_sysmgr_user where tb_sysmgr_user.user_id=tb_exam_student.user_id)" +
    " WHEN  TRIM(student_name)='' THEN (SELECT username FROM tb_sysmgr_user where tb_sysmgr_user.user_id=tb_exam_student.user_id)" +
    " ELSE tb_exam_student.student_name" +
    " end  as student_name,tb_exam_student.user_id" +
    " FROM tb_exam_student) as tb_exam_student on tb_exam_analysis_quescount.user_id= tb_exam_student.user_id" +
    " where tb_exam_student.user_id ='" + userid + "'  ";
    //console.log('sql',sql)

    helper.mysql.query('', sql, function (err, doc) {
        if (err) callback(JSON.stringify({err: 1}), null);
        if (doc) callback(null, JSON.stringify({err: 0, result: doc}));
    })
};


//===================以下为共通辅助方法================================

/**
 * guid
 * @returns {*}
 */
function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + S4() + S4() + S4() + S4() + S4() + S4());
}

/**
 * 日期格式化
 * @param time
 * @returns {string}
 */
function formatTime(time) {
    var now = new Date(time);
    var year = now.getFullYear();
    var month = (now.getMonth() + 1 > 9) ? (now.getMonth() + 1) : ('0' + (now.getMonth() + 1));
    var date = (now.getDate() > 9) ? now.getDate() : ('0' + now.getDate());
    var hour = (now.getHours() > 9) ? now.getHours() : ('0' + now.getHours());
    var minute = (now.getMinutes() > 9) ? now.getMinutes() : ('0' + now.getMinutes());
    var second = (now.getSeconds() > 9) ? now.getSeconds() : ('0' + now.getSeconds());
    return year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
}
