
//登录页面事件注册
function loginformFun() {

    if(getCookie('teacher_remember')) {
        $('.re_me i').toggleClass('checked');
        $("#account").val(getCookie("teacher_account"));
        if (getCookie("teacher_password"))
            $("#password").val(decode64(getCookie('teacher_password')));
    }


    //登录
    $("#loginbtn").on("click",function() {
        if (LoginControlValid() == false) {
            return;
        }
        //收集注册信息
        var logininfo = {};
        logininfo.account = $("#account").val();
        logininfo.password = $("#password").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/admin/login/login',
            data: logininfo,
            success: function (msg) {
                if (msg && msg.err == 0) {
                    if ($(".re_me i").hasClass('checked')) {
                        if(!getCookie("teacher_account")){
                            setCookie("teacher_remember", true);
                            setCookie("teacher_account", logininfo.account);
                            setCookie("teacher_password", encode64(logininfo.password));
                        }
                    } else {
                        delCookie('teacher_remember');
                        delCookie("teacher_account");
                        delCookie("teacher_password");
                    }
                    window.location.href = "/admin/consoledesk";
                }
                else if (msg && msg.err == 2) {
                    //  $("#msg").html('账号或密码错误')
                   // alert($(".re_me i").hasClass())
                    shake_box('账号或密码错误');
                }
                else {
                    shake_box('登录失败');
                }
            }
        });
    });

    $('.shade').click(function(){
        $('.shade').fadeOut();
        $('.shade_input').fadeOut();
    });

    //忘记密码
    $("#forgetpassword").on("click",function(){
        $('.shade').fadeIn();
        $('.shade_input').fadeIn();
        $('.shade').css('width',$(document).width());
        $('.shade').css('height',$(document).height());
    })

    //找回密码发送邮件
    $("#sendcode").on('click',function(){
        var email=$.trim($("#emailtxt").val());
        $.ajax({
            type:'post',
            dataType:'json',
            url:'/admin/login/sendemail',
            data:{email:email},
            success:function(msg){
                console.log('err',msg);
                if(msg&&msg.err == 0){

                    window.location.href = "/admin/resetpwd?email="+email;
                }
                else  if(msg&&msg.err ==2){
                    $(".emial_err").html('你输入的邮箱地址不存在')
                }
                else
                {
                    $(".emial_err").html('邮件发送失败')
                }
            }
        });

    })

    //记住我
    $("#remberme").on("click",function(){

    })
}

//验证页面控件值
function LoginControlValid(){
    var account=$.trim($("#account").val());
    var password=$.trim($("#password").val());
    if(account==""){
        $("#account").addClass('err');
        $("#account").next('em').show();
        return false;
    }

    if(password==""){
        $("#password").addClass('err');
        $("#password").next('em').show();
        return false;
    }
    return true;
}

//=============注册============================
//注册页面事件
function registerformFun(){
    validregister();

    //注册
    $("#regsavebtn").on("click",function(){
        if(regControlValid() == false){
            return;
        }
        //收集注册信息
        var reginfo={};
        reginfo.account=$("#account").val();
        reginfo.fullname=$("#fullname").val();
        reginfo.password=$("#password").val();
        reginfo.schoolname='';
        reginfo.schooltype='';
        reginfo.grade='';
        $.ajax({
            type:'post',
            dataType:'json',
            url:'/admin/register/register',
            data:reginfo,
            success:function(msg){
                console.log('msg',msg);
                if(msg&&msg.err == 1){
                    shake_box('注册成功')
                    window.location.href = "/admin/login";
                }
                else
                {
                    shake_box('注册失败')
                }
            }
        });
    });
}

//验证账号是否已存在
function  validregister()
{
    $('#account').blur(function(){
        checkAccountExit();
    }).focus(function(){
        //checkAccountExit();
    }).keydown(function(){
        //checkAccountExit();
    }).keyup(function(){
        //checkAccountExit();
    });
}

//验证账号是否已存在
function checkAccountExit(){
    var account=$("#account").val();

    var result=true;
    //实时判断是否存在
    $.ajax({
        type:'post',
        dataType:'json',
        url:'/admin/register/checkaccountexist',
        async:false,
        data:{account:account},
        success:function(data){
            //var item=JSON.parse(data);
            if(data&&data.num == 1){
                $("#account").addClass('err');
                $(".email_exist").show();
                result= false;
                return result;
            }
            else{
                $("#account").removeClass('err');
                $(".email_exist").hide();
            }
        }
    });
    return result;
}

//验证控件值
function regControlValid(){

    var validresult=true;

    var account= $.trim($("#account").val());
    var fullname=$.trim($("#fullname").val());
    var password=$.trim($("#password").val());
    var confirmpassword=$.trim($("#confirmpassword").val());

    //账号
    if(account==""){
        $("#account").addClass('err');
        $("#account").next('em').show();
        validresult=false;
        return validresult;
    }
    else{
        $("#account").removeClass('err');
        $("#account").next('em').hide();

        //实时判断是否存在
        $.ajax({
            type:'post',
            dataType:'json',
            url:'/admin/register/checkaccountexist',
            async:false,
            data:{account:account},
            success:function(data){
                if(data&&data.num == 1){
                    $("#account").addClass('err');
                    $(".email_exist").show();
                    validresult=false;
                    return validresult;
                }
                else{
                    $("#account").removeClass('err');
                    $(".email_exist").hide();
                }
            }
        });
    }

    if(checkmail(account)==false){
        $("#account").addClass('err');
        $(".email_valid").show();
        validresult=false;
        return validresult;
    }
    else{
        $("#account").removeClass('err');
        $(".email_valid").hide();
    }

    //姓名
    if(fullname==""){
        $("#fullname").addClass('err');
        $("#fullname").next('em').show();
        validresult=false;
        return validresult;
    }
    else{
        $("#fullname").removeClass('err');
        $("#fullname").next('em').hide();
    }

    //密码
    if(password==""){
        $("#password").addClass('err');
        $("#password").next('em').show();
        validresult=false;
        return validresult;
    }
    else{
        $("#password").removeClass('err');
        $("#password").next('em').hide();
        if(password.length<4){
            $("#password").addClass('err');
            $(".pwd_valid").show();
            validresult=false;
            return validresult;
        }
        else{
            $("#password").removeClass('err');
            $(".pwd_valid").hide();
        }
    }

    if(confirmpassword==""){
        $("#confirmpassword").addClass('err');
        $("#confirmpassword").next('em').show();
        validresult=false;
        return validresult;
    }
    else{
        $("#confirmpassword").removeClass('err');
        $("#confirmpassword").next('em').hide();
    }

    if(confirmpassword !=password){
        $("#confirmpassword").addClass('err');
        $(".confirm_pwd").show();
        validresult=false;
        return validresult;
    }
    else{
        $("#confirmpassword").removeClass('err');
        $(".confirm_pwd").hide();
    }

    return validresult;
}

//验证邮箱格式是否正确
function checkmail(email) {
    var reg  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!reg.test(email)){
        return false;
    }
    return true;
}

//验证密码
function checkPassComplexity(s){
    if(s.length < 4){
        return 0;
    }
    var ls = 0;
    if(s.match(/([a-z])+/)){
        ls++;
    }
    if(s.match(/([0-9])+/)){
        ls++;
    }

    if(s.match(/([A-Z])+/)){
        ls++;
    }
    if(s.match(/[^a-zA-Z0-9]+/)){
        ls++;
    }
    console.log('ls',ls);
    return ls
}

//================个人中心=========================

//常规设置
function personalcenterSettingFun(){

    var input= $('.info_list').eq(0).find('input').val();
    $('.info_list li:lt(1)').find('input').val('').val(input).eq(0).focus();
    $('.info_list input').attr('readonly',null).addClass('active');
    $('.sex_sel i').css('border','2px solid #fdd835');
    $('.sex_sel').find('i').click(function(){
        $(this).addClass('checked').parent().siblings().children('i').removeClass('checked');
    });
    $('.save_btn').show();

    //保存常规设置
    $("#settingsavebtn").on("click",function(){

        var account=$("#account").val();
        var fullname=$("#fullname").val();
        //姓名
        if(account=="") {
            $("#account").addClass('err');
            $("#account").next('em').show();
            return;
        }
        else {
            $("#account").removeClass('err');
            $("#account").next('em').hide();
        }

        if(fullname=="") {
            $("#fullname").addClass('err');
            $("#fullname").next('em').show();
            return;
        }
        else {
            $("#fullname").removeClass('err');
            $("#fullname").next('em').hide();
        }

        //常规设置信息
        var baseinfo={};
        baseinfo.account=account;
        baseinfo.fullname=fullname;
        baseinfo.nickname=$("#nickname").val();
        baseinfo.sex=$(".checked").attr("value");
        baseinfo.birthdayyear=$("#year").val();
        baseinfo.birthdaymonth=$("#month").val();
        baseinfo.birthdayday=$("#day").val();
        //console.log('baseinfo',baseinfo)
        $.ajax({
            type:'post',
            dataType:'json',
            url:'/admin/setting/savegeneralsetting',
            data:baseinfo,
            success:function(msg){
                //console.log('msg',msg);
                if(msg&&msg.err == 1){
                    capacity('保存成功')
                }
                else
                {
                    shake_box('保存失败')
                }
            }
        });
    });
}

//========================学校信息=================================
function shchoolinfoPageFun(){
    $('.change_info').on('click',function(){
        $('.info_list input').attr('readonly',null).addClass('active');
        var input= $('.info_list').eq(0).find('input').val();
        $('.info_list li:lt(1)').find('input').val('').val(input).eq(0).focus();

        $('.birthday .reset').hide();

        $('.birthday span:gt(0) ').css('display','inline-block');

        $('.sex_sel').find('i').click(function(){
            $(this).addClass('checked').parent().siblings().children('i').removeClass('checked');
        });

        $('.cons .gray').hide();
        $('.cons_list').hide();
        $('.blood .gray').hide();
        $('.con_tio').show();
        $('.blood_type').show();
        $('.address').show().val('');
        $('.bo_phone').hide();
        $('.save_btn').show();
        $('.sex_sel i').css('border','2px solid #fdd835');
        $('.sel_sch').attr('readonly','readonly');

        $('.comm').click(function(){
            $(this).find('ul').slideDown(250);
            $(this).parent().siblings('li').find('ul').hide();
            $('.birthday span').find('ul').hide();
            $('.comm_list li').click(function(){
                $(this).parent().prev().html($(this).html()).css('color','#333');
                $('.comm_list').slideUp(250);
                return false;
            });
        });


        $('.check_err').blur(function(){
            if($(this).val()==''){
                $(this).addClass('err');
                $(this).next('em').show();
            }
        }).focus(function(){
            $(this).removeClass('err');
            $(this).next('em').hide();
        });

        $('.birthday span:gt(0)').click(function(){
            $(this).find('ul').slideDown(250);
            $(this).siblings('span').find('ul').slideUp();
            $(this).parent().siblings('li').find('ul').hide();
            $('.school_sel').hide();
            $('.other_school').hide();
            $('.comm_list').hide();
            $('.birthday span').find('li').click(function(){
                var text=$(this).html();
                $(this).parent().prev().html(text).css('color','#333');
                $(this).parent().slideUp(250);
                event.stopPropagation();
            });
        })
        $(this).fadeOut();
    });

}
//=====================安全隐私====================
function securityPageFun(){

    validSecurityPage();

    $('.change_info').fadeOut();
    $('.info_list li').show();
    $('.bo_phone').hide();
    $('.save_btn').show();

    $("#cmpic").on('click',function(){
        $("#cmpic").attr("src","/common/verifyCode?"+Math.random());
    })

    $("#bindmobile").on('click',function(){
        $('.shade').fadeIn();
        $('.shade_input').fadeIn();
        $('.shade').css('width',$(document).width());
        $('.shade').css('height',$(document).height());
    })
    $('.shade').click(function(){
        $('.shade').fadeOut();
        $('.shade_input').fadeOut();
    });
    $("#unbindmobile").on('click',function(){
        var bindmobile=$("#phonenumber").val();
        $("#tel").val(bindmobile)
        $('.shade').fadeIn();
        $('.shade_input').fadeIn();
        $('.shade').css('width',$(document).width());
        $('.shade').css('height',$(document).height());
    })

    //保存安全隐私
    $("#savesecurity").on("click",function(){

        var newpassword=$("#newpassword").val();
        var confirmnewpassword=$("#confirmnewpassword").val();
        var verifcode=$("#verifcode").val();;
        if(newpassword=="") {
            $("#newpassword").addClass('err');
            $("#newpassword").next('em').show();
            return;
        }
        else {
            $("#newpassword").removeClass('err');
            $("#newpassword").next('em').hide();
            if(newpassword.length<4){
                $("#newpassword").addClass('err');
                $(".pwd_valid").show();
                return;
            }
            else{
                $("#newpassword").removeClass('err');
                $(".pwd_valid").hide();
            }
        }

        if(confirmnewpassword=="") {
            $("#confirmnewpassword").addClass('err');
            $("#confirmnewpassword").next('em').show();
            return;
        }
        else {
            $("#confirmnewpassword").removeClass('err');
            $("#confirmnewpassword").next('em').hide();
        }

        if($('#confirmnewpassword').val() != $('#newpassword').val()) {
            $(".confirmnewpassword").addClass('err');
            $(".err_confirmpwd").show();
            return ;
        }
        else{
            $(".confirmnewpassword").removeClass('err');
            $(".err_confirmpwd").hide();
        }

        if(verifcode=="") {
            $("#verifcode").addClass('err');
            $("#verifcode").next('em').show();
            return;
        }
        else {
            $("#verifcode").removeClass('err');
            $("#verifcode").next('em').hide();
        }

        var validresult=true;
        $.ajax({
            type:'post',
            dataType:'json',
            url:'/admin/securitysetting/checkverifcode',
            async:false,
            data:{verifcode:verifcode},
            success:function(data){
                if(data&&data.num == 1){
                    //验证码输入正确
                    $("#verifcode").removeClass('err');
                    $(".err_verifcode").hide();
                }
                else{
                    $("#verifcode").addClass('err');
                    $(".err_verifcode").show();
                    validresult= false;
                    return validresult;
                }
            }
        });
        if(validresult == false){
            return;
        }

        var securityinfo={};
        securityinfo.newpassword=$("#newpassword").val();
        securityinfo.phonenumber=$("#phonenumber").val();

        $.ajax({
            type:'post',
            dataType:'json',
            url:'/admin/securitysetting/savesecuritysetting',
            data:securityinfo,
            success:function(msg){
                //console.log('msg',msg);
                if(msg&&msg.err == 1){
                    capacity('保存成功')
                }
                else
                {
                    shake_box('保存失败')
                }
            }
        });

    })


    /*倒计时*/
    var wait=60;
    function time(o) {
        if (wait == 0) {
            $('.code').removeClass('gray');
            o.removeAttribute("disabled");
            o.value="重新获取";
            wait = 60;
        } else {
            o.setAttribute("disabled", true);
            o.value="已发送(" + wait + ")";
            wait--;
            setTimeout(function() {
                time(o)
            }, 1000)
        }
    }
    $('.code').click(function(){
        var then = this;
        $("#tel_err").html('');
        var tel = $.trim($("#tel").val());
        var tel_result = CheckMobile(tel);
        tel_result = JSON.parse(tel_result);

        if(tel_result && tel_result.err == 0 ){
            $.ajax({
                type: 'POST',
                url: 'http://' + location.host + "/admin/securitysetting/sendsms",
                data: {
                    tel: tel
                },
                dataType: "json",
                success: function (msg) {
                    if(msg.err == 0){
                        $("#tel_err").html('验证码发送成功');
                        time(then);
                        $(then).addClass('gray');
                    }else{
                        $("#tel_err").html('验证码发送失败,请稍后再试');
                    }
                }
            });
        }else{
            $("#tel_err").html(tel_result.message);
        }
    });
    /**
     * 绑定手机号码
     */
    $("#bind_mobile").click(function () {
        var isok = true;

        $("#tel_err").html('');
        var tel = $.trim($("#tel").val());
        var tel_result = CheckMobile(tel);
        tel_result = JSON.parse(tel_result);
        if (tel_result && tel_result.err != 0) {
            $("#tel_err").html(tel_result.message);
            isok = false;
        }

        var VerificationCode = $.trim($("#VerificationCode").val());
        if (!VerificationCode) {
            $("#code_err").html('请输入验证码');
            isok = false;
        }

        if (!isok) return;
        //$('.loadingBox').show();
        $.ajax({
            type: 'POST',
            url: 'http://' + location.host + "/admin/securitysetting/BindTel",
            data: {
                tel: tel,
                code: VerificationCode,
            },
            dataType: "json",
            success: function (msg) {
                $('.loadingBox').hide();
                if (msg.err == 0) {
                    $('.shade').fadeOut();
                    $('.shade_input').fadeOut();
                    setTimeout(function () {
                        //shake_box('绑定手机号码成功');
                        $("#phonenumber").val(tel);
                    }, 500);
                    setTimeout(function () {
                        //window.location.href = "http://" + location.host + "/center?callback=/center/setting/security";
                    }, 1500);
                } else if (msg.err == 5) {
                    $('.shade').fadeOut();
                    $('.shade_input').fadeOut();
                    setTimeout(function () {
                        //shake_box("回话已过期,请重新登录");
                    }, 500);
                } else if (msg.err == 6 || msg.err == 7) {
                    $("#code_err").html(msg.message);
                } else {
                    $("#code_err").html('验证码发送失败,请稍后再试');
                }
            }
        });
    });

    /**
     * 解除绑定手机号码
     */
    $("#unbind_mobile").click(function () {
        var isok = true;

        $("#tel_err").html('');
        var tel = $.trim($("#tel").val());
        var tel_result = CheckMobile(tel);
        tel_result = JSON.parse(tel_result);
        if (tel_result && tel_result.err != 0) {
            $("#tel_err").html(tel_result.message);
            isok = false;
        }

        var VerificationCode = $.trim($("#VerificationCode").val());
        if (!VerificationCode) {
            $("#code_err").html('请输入验证码');
            isok = false;
        }

        if (!isok) return;
        $('.loadingBox').show();
        $.ajax({
            type: 'POST',
            url: 'http://' + location.host + "/admin/securitysetting/unBindTel",
            data: {
                tel: tel,
                code: VerificationCode
            },
            dataType: "json",
            success: function (msg) {
                $('.loadingBox').hide();
                if (msg.err == 0) {
                    $('.shade').fadeOut();
                    $('.shade_input').fadeOut();
                    setTimeout(function () {
                        //shake_box('解除绑定手机号码成功');
                        $("#phonenumber").val('');
                    }, 500);
                    setTimeout(function () {
                        //window.location.href = "http://" + location.host + "/center?callback=/center/setting/security";
                    }, 1500);
                } else if (msg.err == 5) {
                    $('.shade').fadeOut();
                    $('.shade_input').fadeOut();
                    setTimeout(function () {
                        //shake_box("回话已过期,请重新登录");
                    }, 500);
                } else if (msg.err == 6) {
                    $("#code_err").html(msg.message);
                } else {
                    $("#code_err").html('解除绑定手机号码成功失败,请稍后再试');
                }
            }
        });
    });
}

//验证账号是否已存在
function  validSecurityPage()
{
    $('#password').blur(function(){
        checkPassword();
    }).focus(function(){
    }).keydown(function(){
    }).keyup(function(){
    });

    //$('#phonenumber').blur(function(){
    //    checkMobileBind();
    //}).focus(function(){
    //}).keydown(function(){
    //}).keyup(function(){
    //});

    $('#verifcode').blur(function(){
        checkverifcode();
    }).focus(function(){
    }).keydown(function(){
    }).keyup(function(){
    });

    $('#confirmnewpassword').blur(function(){
        confirmnewpassword();
    }).focus(function(){
        confirmnewpassword();
    }).keydown(function(){
    }).keyup(function(){
    });
}

/*
  验证两次输入密码是否一致
 */
function confirmnewpassword(){
    if($('#confirmnewpassword').val() !=$('#newpassword').val()) {
        $(".confirmnewpassword").addClass('err');
        $(".err_confirmpwd").show();
        return false;
    }
    else{
        $(".confirmnewpassword").removeClass('err');
        $(".err_confirmpwd").hide();
        return true;
    }
}


//验证输入密码是否正确
function checkPassword(){
    var password=$("#password").val();
    //实时判断是否存在
    $.post("/admin/securitysetting/checkpassword",{password:password},function(data){
        var item=JSON.parse(data);
        if(item&&item.num == 1){
            $("#password").removeClass('err');
            $(".err_pwd").hide();
            return true;
        }
        else{
            //原始密码输入正确
            $("#password").addClass('err');
            $(".err_pwd").show();
            return false;
        }
    });
}

/*
判断手机号是否已绑定
 */
function checkMobileBind(){
    //var phonenumber=$("#phonenumber").val();
    ////实时判断是否存在
    //$.post("/admin/securitysetting/checkMobileIsBind",{phonenumber:phonenumber},function(data){
    //    var item=JSON.parse(data);
    //    if(item&&item.num == 1){
    //        //手机号已绑定
    //        $("#phonenumber").addClass('err');
    //        $(".binded_mobile").show();
    //        return false;
    //    }
    //    else{
    //        $("#phonenumber").removeClass('err');
    //        $(".binded_mobile").hide();
    //        return true;
    //    }
    //});
}

/*
判断验证码输入是否正确
 */
function checkverifcode(){
    var verifcode=$("#verifcode").val();
    //实时判断是否存在
    $.post("/admin/securitysetting/checkverifcode",{verifcode:verifcode},function(data){
        var item=JSON.parse(data);
        //console.log('item',item)
        if(item&&item.num == 1){
            //验证码输入正确
            $("#verifcode").removeClass('err');
            $(".err_verifcode").hide();
            return true;
        }
        else{
            $("#verifcode").addClass('err');
            $(".err_verifcode").show();
            return false;
        }
    });
}

/**
 * 手机号码验证
 * @param tel
 * @constructor
 */
function CheckMobile(tel) {
    if (!tel) {
        return JSON.stringify({err: 1, message: '请输入您的手机号码'});
    } else {
        if (tel.length != 11 || !/^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1})|147)+\d{8})$/.test(tel)) {
            return JSON.stringify({err: 1, message: '您输入的手机号码有误,请重新输入'});
        } else {
            return JSON.stringify({err: 0})
        }
    }
}


//=====================邮箱找回密码======================
function resetpasswordPage(){

    $('.check_err').blur(function(){
        if($(this).val()==''){
            $(this).addClass('err');
            $(this).next('em').show();
        }
    }).focus(function(){
        $(this).removeClass('err');
        $(this).next('em').hide();
    });


    //保存
    $("#savebtn").on("click",function(){

        var password=$("#password").val();
        var newpassword=$('#confirmpassword').val();

        if(password ==""){
            $("#password").addClass('err');
            $("#password").next('em').show();
            return;
        }
        else{
            $("#password").removeClass('err');
            $("#password").next('em').hide();
        }

        if(newpassword ==""){
            $("#confirmpassword").addClass('err');
            $("#confirmpassword").next('em').show();
            return;
        }
        else{
            $("#confirmpassword").removeClass('err');
            $("#confirmpassword").next('em').hide();
        }

        if($("#validcode").val() ==""){
            $("#validcode").addClass('err');
            $("#validcode").next('em').show();
            return;
        }
        else{
            $("#validcode").removeClass('err');
            $("#validcode").next('em').hide();
        }

        if(confirmresetpassword()==false){
            return;
        }

        if(checkresetpwdverifcode() ==false){
            return;
        }

        var email=getQueryString('email');

        $.ajax({
            type:'post',
            dataType:'json',
            url:'/admin/resetpwd/updatepassword',
            data:{password:newpassword,email:email},
            success:function(data){
                console.log('data',data)
                if(data&&data.err==1 ){
                    window.location.href = "/admin/login";
                }
                else
                {
                    $("#msg").html('保存失败')
                    return  false;
                }
            }
        });
    })

}

/*
 验证两次输入密码是否一致
 */
function confirmresetpassword(){
    if($('#password').val() !=$('#confirmpassword').val()) {
        $(".confirmpassword").addClass('err');
        $(".err_confirmpwd").show();
        return false;
    }
    else{
        $(".confirmpassword").removeClass('err');
        $(".err_confirmpwd").hide();
        return true;
    }
}

/**
 * 找回密码验证码是否正确
 */
function checkresetpwdverifcode(){
    var verifcode=$("#verifcode").val();
    var email=getQueryString('email');
    //实时判断是否存在
    var result=true;
    $.ajax({
        type:'post',
        dataType:'json',
        url:'/admin/resetpwd/checkverifcode',
        data:{verifcode:verifcode,email:email},
        async:false,
        success:function(item){
            //console.log('item',item)
            if(item&&item.err == 6){
                $("#verifcode").addClass('err');
                $(".err_verifcode").show();
                $(".err_verifcode").html(item.message);
                result=false;
                return false;
            }
            else{
                //验证码输入正确
                $("#verifcode").removeClass('err');
                $(".err_verifcode").hide();
                result=true;
                return true;
            }
        }
    });
    return result;
}

//==================控制台================================

function  initconsoledeskFun(){

    var defaultgradeid=$("#grade").val();
    if (defaultgradeid && defaultgradeid != '0') {
        $("#point").html('<option value="0">请选择知识点</option>');
        $("#classnum").html('<option value="0">请选择班级</option>');

        $.ajax({
            type: "POST",
            url: "http://" + location.host + "/admin/consoledesk/getPoints",
            data: {
                gradeid: defaultgradeid,
            },
            success: function (data) {
                var html = "";
                if (data.pointsarray && data.pointsarray.length > 0) {
                    for (var i = 0; i < data.pointsarray.length; i++) {
                        if (i == 0) {
                            html += '<option selected="selected" value="' + data.pointsarray[i].point_id + '">' + data.pointsarray[i].point_name + '</option>'
                        }
                        else {
                            html += '<option value="' + data.pointsarray[i].point_id + '">' + data.pointsarray[i].point_name + '</option>'
                        }
                    }
                }
                $("#point").append(html);


                var classhtml = "";
                if (data.classnumArray && data.classnumArray.length > 0) {
                    for (var index = 0; index < data.classnumArray.length; index++) {
                        if (index == 0) {
                            classhtml += '<option selected="selected"  value="' + data.classnumArray[index] + '">' + data.classnumArray[index] + '</option>'

                        } else {
                            classhtml += '<option value="' + data.classnumArray[index] + '">' + data.classnumArray[index] + '</option>'
                        }
                    }
                }
                $("#classnum").append(classhtml)
                initEchartsFun();
                initanxietyechartsFun();
            },
            dataType: "json"
        });
    }

    $("#grade").change(function () {
        $("#point").html('<option value="0">请选择知识点</option>');
        $("#classnum").html('<option value="0">请选择班级</option>');
        var gradeid = $(this).val();
        //console.log('gradeid',gradeid);
        if (gradeid && gradeid != '0') {
            $.ajax({
                type: "POST",
                url: "http://" + location.host + "/admin/consoledesk/getPoints",
                data: {
                    gradeid: gradeid,
                },
                success: function (data) {
                    //console.log('data',data);

                    var html = "";
                    if (data.pointsarray && data.pointsarray.length > 0) {
                        for (var i = 0; i < data.pointsarray.length; i++) {
                            html += '<option value="' + data.pointsarray[i].point_id + '">' + data.pointsarray[i].point_name + '</option>'
                        }
                    }
                    $("#point").append(html);

                    var classhtml = "";
                    if (data.classnumArray && data.classnumArray.length > 0) {
                        for (var i = 0; i < data.classnumArray.length; i++) {
                            classhtml += '<option value="' + data.classnumArray[i] + '">' + data.classnumArray[i] + '</option>'
                        }
                    }
                    $("#classnum").append(classhtml)

                },
                dataType: "json"
            });

            initanxietyechartsFun();
        }
    });
}



function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}