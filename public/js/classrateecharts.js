// 基于准备好的dom，初始化echarts实例
var classChart = echarts.init(document.getElementById('classeschart'));
var userchart = echarts.init(document.getElementById('userchart'));
var useranxietychart = echarts.init(document.getElementById('anxietychart'));

function  initclassrateEcharts(classnumarray,classratearray){

   var option =  {
       title:'',
       color: ['#3398DB'],
       tooltip : {
           trigger: 'axis',
           axisPointer : {            // 坐标轴指示器，坐标轴触发有效
               type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
           }
       },
       grid: {
           left: '3%',
           right: '4%',
           bottom: '3%',
           containLabel: true
       },
       xAxis : [
           {
               type : 'category',
               data : classnumarray,
               axisTick: {
                   alignWithLabel: true
               },
               axisLine: {
                   lineStyle: {
                       color: '#333',
                       width:0
                   }
               },
           }
       ],
       yAxis : [
           {
               type : 'value',max:100,
               splitLine: {
                   lineStyle: {
                       type: 'dashed',
                       color:'#cccccc'
                   }
               },
               axisLine: {
                   lineStyle: {
                       color: '#999',
                       width:0
                   }
               },
           }
       ],
       series : [
           {
               name:'准确率',
               barWidth: 20,
               type:'bar',
               data:classratearray
           }
       ],
       tooltip: {
        trigger: 'item',
            backgroundColor: 'rgba(0,0,0,0)',
            formatter: function (params) {
            return "准确率:"+params.value+"%";
        },
        show: true,
            showContent: true,
            showDelay: 20,
            hideDelay: 100,
            transitionDuration: 0.4,
            enterable: false,
            backgroundColor: 'rgba(0,0,0,0.7)',
            borderColor: '#333',
            borderRadius: 4,
            borderWidth: 0,
    }
   };
    classChart.setOption(option,true,true,true);
}
function  inituserrateEcharts(userarray,ratearray){
    var option = {
        title : {
            text: ''
        },
        grid: {
            left: '3%',
            right: '7%',
            bottom: '3%',
            containLabel: true
        },
        tooltip : {
            trigger: 'axis',
            showDelay : 0,
            //formatter : function (params) {
            //    if (params.value.length > 1) {
            //        return params.seriesName + ' :<br/>'
            //            + params.value[0] + 'cm '
            //            + params.value[1] + 'kg ';
            //    }
            //    else {
            //        return params.seriesName + ' :<br/>'
            //            + params.name + ' : '
            //            + params.value + 'kg ';
            //    }
            //},
            axisPointer:{
                show: true,
                type : 'cross',
                lineStyle: {
                    type : 'dashed',
                    width : 1
                }
            }
        },
        toolbox: {
            feature: {
                dataZoom: {},
                brush: {
                    type: ['rect', 'polygon', 'clear']
                }
            }
        },
        brush: {
        },
        legend: {
            data: [],
            left: 'center'
        },
        xAxis : [
            {
                type : 'category',
                axisLabel : {
                    formatter: '{value}'
                },
                splitLine: {
                    show: false
                },
                data :userarray
            }
        ],
        yAxis : [
            {
                type : 'value',
                axisLabel : {
                    formatter: '{value}'
                },
                splitLine: {
                    lineStyle: {
                        type: 'dashed',
                        color:'#ccc'
                    }
                },max:100
            }
        ],
        series : [
            {
                name:'',
                type:'scatter',
                data: ratearray,
                markArea: {
                    silent: true,
                    itemStyle: {
                        normal: {
                            color: 'transparent',
                            borderWidth: 1,
                            borderType: 'dashed'
                        }
                    }
                }
            }
        ]
    };
    userchart.setOption(option,true,true,true);
}

$("#point").change(function () {
    initEchartsFun();
});

function  initEchartsFun()
{
    var pointid = $("#point").val();
    var gradeid = $("#grade").val();
    var classnum= $("#classnum").val();
    if (pointid && pointid != '0') {
        //班级准确率
        $.ajax({
            type: "POST",
            //dataType:'json',
            url: "/admin/consoledesk/getClassesPointrate",
            data: {
                gradeid: gradeid,
                pointid:pointid
            },
            success: function (result) {
                result = JSON.parse(result);

                //console.log('result',result)
                if(result && result.classRate && result.classRate.length>0){
                    var classnumarray = [];//班级
                    var classratearray=[];//准确率
                    var ratelist=result.classRate;

                    if(ratelist&&ratelist.length<=0){
                        classnumarray.push( '');
                        classratearray.push(0);
                    }
                    else{
                        for(var i =0;i<ratelist.length;i++) {
                            classnumarray.push(ratelist[i].classnum+'班');
                            if(ratelist[i].rate){
                                classratearray.push(ratelist[i].rate);
                            }
                            else{
                                classratearray.push(0);
                            }
                        }
                    }
                    initclassrateEcharts(classnumarray,classratearray);
                }
                else {
                    initclassrateEcharts([''],[0]);
                }
            }
        });

        //学生准确率
        if(classnum && classnum!='0'){
            $.ajax({
                type: "POST",
                url: "/admin/consoledesk/getUserPointrate",
                data: {
                    gradeid: gradeid,
                    pointid:pointid,
                    classnum:classnum
                },
                success: function (result) {
                    result = JSON.parse(result);
                    if(result && result.userRate && result.userRate.length>0){
                        var userarray = [];//班级
                        var ratearray=[];//准确率
                        var ratelist=result.userRate;

                        if(ratelist&&ratelist.length<=0){
                            userarray.push( '');
                            ratearray.push(0);
                        }
                        else{
                            for(var i =0;i<ratelist.length;i++) {
                                userarray.push(ratelist[i].studentname);
                                if(ratelist[i].rate){
                                    ratearray.push(ratelist[i].rate);
                                }
                                else{
                                    ratearray.push(0);
                                }
                            }
                        }

                        inituserrateEcharts(userarray,ratearray);
                    }
                    else {
                        inituserrateEcharts([''],[0]);
                    }
                }
            });
        }
        else{
            inituserrateEcharts([''],[0]);
        }
    }
    else{
        initclassrateEcharts([''],[0]);
        inituserrateEcharts([''],[0]);
    }
}

function  initanxietyechartsFun() {
    var gradeid = $("#grade").val();
    if (gradeid && gradeid != '0') {

        var itemStyle = {
            normal: {
                opacity: 0.8,
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowOffsetY: 0
            }
        };

        $.ajax({
            type: "POST",
            url: "/admin/anxiety/getStudentAnxiety",
            data: {
                gradeid: gradeid
            },
            dataType: "json",
            success: function (result) {
                //console.log('userratedata',result.studentAnxietyArray);
                //result = JSON.parse(result);
                if (result && result.studentAnxietyArray && result.studentAnxietyArray.length > 0) {
                    var classarray = result.classnumArray;//班级
                    var maxstudentsnumber=result.maxstudentsnumber;
                    if(maxstudentsnumber <8){
                        maxstudentsnumber=7;
                    }
                    var anxietyarray = [];//焦虑值
                    var anxietylist = result.studentAnxietyArray;

                    var dataArray=[];
                    var dataObject={};
                    if (anxietylist && anxietylist.length <= 0) {
                        //dataObject.name="";
                        //dataObject.type='scatter';
                        //dataObject.itemStyle=itemStyle;
                        //dataObject. data=dataArray;
                        //anxietyarray.push(dataObject);
                    }
                    else {
                        for (var i = 0; i < anxietylist.length; i++) {
                            dataArray=[];
                            dataObject={};
                            var indexanxietyArray=anxietylist[i].anxietyArray;
                            if(indexanxietyArray&&indexanxietyArray.length>0){
                                for(var anietyindex=0;anietyindex<indexanxietyArray.length;anietyindex++){
                                    var anxietyscore=indexanxietyArray[anietyindex].math_anxiety_score;
                                    if (anxietyscore) {
                                        dataArray.push([anietyindex+1,anxietyscore,anxietyscore,0,0,0,50,indexanxietyArray[anietyindex].student_name]);
                                    }
                                    else {
                                        anxietyarray.push([anietyindex+1,0,0,0,0,0,50,'']);
                                    }
                                }
                                dataObject.name=anxietylist[i].classnum+"班";
                                dataObject.type='scatter';
                                dataObject.itemStyle=itemStyle;
                                dataObject. data=dataArray;

                                anxietyarray.push(dataObject)
                            }
                        }
                    }
                    //console.log('classarray',classarray);
                    //console.log('anxietyarray',anxietyarray);
                    initanxietyEcharts(classarray, anxietyarray,maxstudentsnumber);
                }
                else {
                    initanxietyEcharts([''], [0],7);
                }
            }
        });
    }
    else {
        shake_box('请选择年级！');
    }
}

$(document).ready(function() {
    //initEchartsFun();
});