/**
 * Created by Administrator on 2016/9/26 0026.
 */

$(function(){
    /*个人中心 常规设置 */

    $('.change_info').on('click',function(){
       $('.info_list input').attr('readonly',null).addClass('active');
        var input= $('.info_list').eq(0).find('input').val();
        $('.info_list li:lt(1)').find('input').val('').val(input).eq(0).focus();

        $('.birthday .reset').hide();

        $('.birthday span:gt(0) ').css('display','inline-block');

        $('.sex_sel').find('i').click(function(){
            $(this).addClass('checked').parent().siblings().children('i').removeClass('checked');
        });

        $('.cons .gray').hide();
        $('.cons_list').hide();
        $('.blood .gray').hide();
        $('.con_tio').show();
        $('.blood_type').show();
        $('.address').show().val('');
        $('.bo_phone').hide();
        $('.save_btn').show();
        $('.sex_sel i').css('border','2px solid #fdd835');
        $('.sel_sch').attr('readonly','readonly');

     $('.comm').click(function(){
         $(this).find('ul').slideDown(250);
         $(this).parent().siblings('li').find('ul').hide();
         $('.birthday span').find('ul').hide();
         $('.comm_list li').click(function(){
             $(this).parent().prev().html($(this).html()).css('color','#333');
             $('.comm_list').slideUp(250);
             return false;
         });
     });


   $('.check_err').blur(function(){
       if($(this).val()==''){
           $(this).addClass('err');
           $(this).next('em').show();
       }
   }).focus(function(){
       $(this).removeClass('err');
       $(this).next('em').hide();
   });

        $('.birthday span:gt(0)').click(function(){
             $(this).find('ul').slideDown(250);
            $(this).siblings('span').find('ul').slideUp();
            $(this).parent().siblings('li').find('ul').hide();
            $('.school_sel').hide();
            $('.other_school').hide();
            $('.comm_list').hide();
            $('.birthday span').find('li').click(function(){
                var text=$(this).html();
                $(this).parent().prev().html(text).css('color','#333');
                $(this).parent().slideUp(250);
                event.stopPropagation();
            });
        })
   $(this).fadeOut();

        /*添加班级*/

        $('.sel_sch').click(function(){
            $('.other_school').find('input').removeClass('active');
            $('.school_sel').fadeIn();
            $('.other_school').fadeIn();
        });

});

    /* 点空白位置-下拉选择框隐藏  */
    $(document).on("click",function(event){
        var _con = $('.info_list li span');
        if(!_con.is(event.target) && _con.has(event.target).length === 0){
            $(".info_list li").find('ul').hide();
        }

    });



/* 个人中心-学校信息 */

    /*添加学校*/

    $('.school_sel').find('li').click(function(){
       $('.sel_sch').val($(this).children('a').html());
       $(this).parent().parent() .hide();
        $('.other_school').hide();
    });
    $('.btn_selector').click(function(){
        if($('.oth_cs').val()==''){
            alert('请输入学校名称！')
        }else{
            $('.sel_sch').val($('.oth_cs').val());
            $('.school_sel').hide();
            $('.other_school').hide();
            $('.oth_cs').val('');
        }
    });

   $('.close_box').click(function(){
       $('.school_sel').fadeOut();
       $('.other_school').fadeOut();
   });


    /* --添加班级 */

    //$('.add_class_mes').on('click',function(){
    //   $(this).before($(this).prev().clone(true));
    //});




/* 个人中心页-安全隐私 */
    $('.change_info').click(function(){
       $('.change_info').fadeOut();
       $('.info_list li').show();
        $('.bo_phone').hide();
    });


/* 绑定手机号 */

    $( '.go_bound').click(function(){
        $('.shade').fadeIn();
        $('.shade_input').fadeIn();
        $('.shade').css('width',$(document).width());
        $('.shade').css('height',$(document).height());
    });
    $('.shade').click(function(){
        $('.shade').fadeOut();
        $('.shade_input').fadeOut();
    });

    /* 倒计时 */

    $('.code').click(function(){
        time(this);
    });
    var wait=60;
    function time(o) {
        if (wait == 0) {
            $('.code').removeClass('gray');
            o.removeAttribute("disabled");
            o.value="重新获取";
            wait = 60;
        } else {
            o.setAttribute("disabled", true);
            $('.code').addClass('gray');
            o.value="已发送(" + wait + ")";
            wait--;
            setTimeout(function() {
                time(o)
            }, 1000)
        }
    }


    $("#bind_mobile").click(function () {
      if($('#tel').val()==''){
          $('#tel_err').html('手机号不能为空！');
      }
        if($('#VerificationCode').val()==''){
          $('#code_err').html('验证码不能为空！');
      }
    });


    /* 个人中心页-学生管理页 */

    /* 移除学生-单个移除 */
     function  SelectRemove(the){
         var del = window.confirm("确定移除吗?");
         if(del==true){
             $(the).parent().parent().remove();
         }else{
             return true;
         }
         //Libgcolor();
     };

    $('.remove').click(function(){
       var _this= $(this).parent().siblings('.stu_name').find('i');
         if( _this.hasClass('checked')){
             $(this).parent().parent().remove();
          //   Libgcolor();
         }else{
             SelectRemove(this);
         }
    });

  /* 勾选 */
    $('.stu_list li i').click(function(){
        if($(this).hasClass('checked')){
            $(this).removeClass('checked');
        }else{
            $(this).addClass('checked');
        }
    });

     /* 移出学生---勾选后 */
      function Remove(stu) {
           var sel_i = $(stu);
           sel_i.each(function (index) {
               if (sel_i.eq(index).hasClass('checked')) {
                   sel_i.eq(index).parent().parent().remove();
               }
           });
       };

        function select(btn){
            if($(btn).hasClass('checked')){
                Remove(btn);
            }else{
                alert('请至少选择一名学生');
                return false;
            }
        }

           $('.le_remove_btn').click(function () {
               select('.left_stu .stu_list i');
               Libgcolor();
           });

            $('.ri_remove_btn').click(function () {
                select('.right_stu .stu_list i');
            });



  /* Li背景色 */
   function Libgcolor(){
       if(true){
           $('.left_stu .stu_list li:even').css('background','#fffde7');
           $('.left_stu .stu_list li:odd').css('background','#fff');
       }else{
           $('.right_stu .stu_list li:even').css('background','#fffde7');
           $('.right_stu .stu_list li:odd').css('background','#fff');
       }
   }
   Libgcolor(true);




  /* 添加学生 */

    $( '.add_btn').click(function(){
        $('.shade').fadeIn();
        $('.add_stu').fadeIn();
        $('.shade').css('width',$(document).width());
        $('.shade').css('height',$(document).height());
    });
    $('.shade').click(function(){
        $('.shade').fadeOut();
        $('.shade_input').fadeOut();
    });

   /* 登录注册页 */

    /* 验证判断 */
    $('.check_err').blur(function(){
        if($(this).val()==''){
            $(this).addClass('err');
            $(this).next('em').show();
        }
    }).focus(function(){
        $(this).removeClass('err');
        $(this).next('em').hide();
    });

    /*勾选*/
    $('.re_me i').click(function(){
        $(this).toggleClass('checked');
    });

    /*下拉框选项*/
    $('.blood_type').on('click',function(){
        $('.add_class').slideDown();
        $(this).parent().siblings().find('ol').hide();
        $('.add_class li').click(function(){
           $(this).parent().prev().html($(this).html()) ;
            $(this).parent().hide();
            return false;
        });
    });
    $('.cla').on('click',function(){
        $(this).find('ol').slideDown();
        $(this).siblings().find('ol').hide();
        $('.cla_class li').click(function(){
            $(this).parent().prev().html($(this).html()) ;
            $(this).parent().hide();
            return false;
        });
    });

    /* 点空白位置-下拉选择框隐藏  */
    $(document).on("click",function(event){
        var _con = $('.reg_list li').children('span');
        if(!_con.is(event.target) && _con.has(event.target).length === 0){
             $('.add_class').hide();
            $('.cla_class').hide();
        }

    });
});