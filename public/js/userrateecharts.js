// 基于准备好的dom，初始化echarts实例
var userchart = echarts.init(document.getElementById('userchart'));
function  inituserrateEcharts(userarray,ratearray){
    var option = {
        title : {
            text: ''
        },
        color: [
            '#07aaf4'
        ],
        grid: {
            left: '0%',
            right: '3%',
            bottom: '3%',
            containLabel: true
        },
        tooltip : {
            trigger: 'axis',
            showDelay : 0,
            formatter : function (params) {
                if (params.value.length > 1) {
                    return params.value[0] + ' <br/>'
                        +'准确率：'+ params.value[1] + '% ';
                }
                else {
                    return params.name + ' <br/>'
                        +'准确率：'+ params.value + '% ';
                }
            },
            axisPointer:{
                show: true,
                type : 'cross',
                lineStyle: {
                    type : 'dashed',
                    width : 1
                }
            }
        },
        toolbox: {
            feature: {
                dataZoom: {},
                brush: {
                    type: ['rect', 'polygon', 'clear']
                }
            }
        },
        brush: {
        },
        legend: {
            data: [],
            left: 'center'
        },
        xAxis : [
            {
                type : 'category',
                axisLabel : {
                    formatter: '{value}'
                },
                splitLine: {
                    show: false
                },
                axisLine: {
                    lineStyle: {
                        color: '#333',
                        width:0
                    }
                },
                data :userarray
            }
        ],
        yAxis : [
            {
                type : 'value',
                axisLabel : {
                    formatter: '{value}'
                },
                splitLine: {
                    lineStyle: {
                        type: 'dashed',
                        color:'#ccc'
                    }
                },
                nameTextStyle:{
                    color:'#f60'
                },
                axisLine: {
                    lineStyle: {
                        color: '#999',
                        width:0
                    }
                },max:100
            }
        ],
        series : [
            {
                name:'',
                type:'scatter',
                data: ratearray,
                markArea: {
                    silent: true,
                    itemStyle: {
                        normal: {
                            color: 'transparent',
                            borderWidth: 1,
                            borderType: 'dashed'
                        }
                    }
                }
            }
        ]
    };
    userchart.setOption(option,true,true,true);
}

$("#classnum").change(function () {

    inituserratechartsFun();
});

function  inituserratechartsFun() {
    var pointid = $("#point").val();
    var gradeid = $("#grade").val();
    var classnum = $("#classnum").val();

    if (pointid && pointid != '0') {
        $.ajax({
            type: "POST",
            url: "/admin/consoledesk/getUserPointrate",
            data: {
                gradeid: gradeid,
                pointid: pointid,
                classnum: classnum
            },
            success: function (result) {
                //console.log('userratedata',result);
                result = JSON.parse(result);
                if (result && result.userRate && result.userRate.length > 0) {
                    var userarray = [];//班级
                    var ratearray = [];//准确率
                    var ratelist = result.userRate;

                    if (ratelist && ratelist.length <= 0) {
                        userarray.push('');
                        ratearray.push(0);
                    }
                    else {
                        for (var i = 0; i < ratelist.length; i++) {
                            userarray.push(ratelist[i].studentname);
                            if (ratelist[i].rate) {
                                ratearray.push(ratelist[i].rate);
                            }
                            else {
                                ratearray.push(0);
                            }
                        }
                    }
                    inituserrateEcharts(userarray, ratearray);
                }
                else {
                    inituserrateEcharts([''], [0]);
                }
            }
        });
    }
    else {
        shake_box('请选择知识点！');
    }
}

$(document).ready(function() {
    //inituserrateEcharts([''],[0]);
});