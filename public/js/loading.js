function shake_box(message){
    $('.shade_box .cont').children('h2').html(message);
    $('.shade').fadeIn(100).delay(1000).fadeOut();
    $('.shade_box').fadeIn(100).delay(1000).fadeOut();
    $('.shade').css('width',$(document).width());
    $('.shade').css('height',$(document).height());
}
function capacity(message){
    $('.save_btn_ico').html(message);
  $('.save_btn_ico').stop(true,true).slideDown(250).delay(1000).fadeOut();
}