window.onload = function () {
    var load_city_id = $.trim($("#result_city_id").val());
    var load_province_id = $.trim($("#result_province_id").val());
    var load_area_id = $.trim($("#result_area_id").val());
    var load_school_id = $.trim($("#school_id").val());
    var load_school_name = $.trim($('#school_name').val());
    var teacherclasses = JSON.parse($('#teacherclasses').val());
    if (load_province_id && load_city_id && load_area_id && load_school_id && load_school_name) {
        CityList(true,load_city_id, load_province_id);
        AreaList(true,load_area_id, load_city_id);
        GetSchool(load_area_id);
        var teacherclasses_result = GetGradeClasses(load_school_id, load_school_name);
        var list = [];
        if (teacherclasses_result && teacherclasses && teacherclasses.length > 0) {
            for (var j = 0; j < teacherclasses.length; j++) {
                var gradeid=teacherclasses[j].grade_id;
                var classnum=teacherclasses[j].class_num;

                var allGradeArray=[];
                for (var i = 0; i < teacherclasses_result.length; i++) {
                    allGradeArray.push({"grade_id":teacherclasses_result[i].grade_id,"grade_name":teacherclasses_result[i].grade_name});
                }
                teacherclasses[j].allGradeList=allGradeArray;

                _gradeId=gradeid;
                var findGradeResult=teacherclasses_result.find(findGradeIdExist);
                if (findGradeResult == undefined) {
                }
                else{
                    var indexclassnumlist=findGradeResult.classes_list;
                    var allClassnumArray=[];
                    for (var k = 0; k < indexclassnumlist.length; k++) {
                        allClassnumArray.push({"class_num":indexclassnumlist[k].class_num});
                    }
                    teacherclasses[j].allClassNumList=allClassnumArray;
                }
            }
            list= teacherclasses;

            if (list && list.length > 0) {
                $("#pargradediv").remove();
                for (var index = 0; index < list.length; index++) {

                    var html = "<div style='margin-left:60px;line-height:60px;position: relative;'>"+"<img src='../images/dele_ico.png' style='position: absolute;left: 280px;top:16px; cursor: pointer;' class='close_btn'/> ";
                    var html_grade = "<select id='grade' onchange='gradechangefun(this)'style='margin-right: 20px;'><option value='-1'>请选择年级</option>";
                    for(var childindex=0;childindex<list[index].allGradeList.length;childindex++){
                        if(list[index].allGradeList[childindex].grade_id == list[index].grade_id ){
                            html_grade += "<option selected='selected' value='" + list[index].allGradeList[childindex].grade_id + "'>" + list[index].allGradeList[childindex].grade_name + "</option>";
                        }
                        else{
                            html_grade += "<option value='" + list[index].allGradeList[childindex].grade_id + "'>" + list[index].allGradeList[childindex].grade_name + "</option>";
                        }
                    }
                    html_grade+="</select>";

                    var html_class = " <select id='class_num'><option value='-1'>请选择班级</option>";
                    for(var childClassnumindex=0;childClassnumindex<list[index].allClassNumList.length;childClassnumindex++){
                        if(list[index].allClassNumList[childClassnumindex].class_num == list[index].class_num){
                            html_class += "<option selected='selected' value='" + list[index].allClassNumList[childClassnumindex].class_num + "'>" + list[index].allClassNumList[childClassnumindex].class_num + "</option>";
                        }
                        else{
                            html_class += "<option value='" + list[index].allClassNumList[childClassnumindex].class_num + "'>" + list[index].allClassNumList[childClassnumindex].class_num + "</option>";
                        }
                    }
                    html_class += "</select>";

                    html += html_grade;
                    html += html_class;
                    html += "</div>";

                    $("#grade_span").after(html);
                }
            }
        }
    }
};

var _gradeId='';
function findGradeIdExist(gradeinfo) {
    return gradeinfo.grade_id === _gradeId;
}

var _classnum='';
function findClassNumExist(classinfo) {
    return classinfo.class_num === _classnum;
}

function CityList(state,cityid,province_id){
    var result = GetCityByProvinceId(province_id);
    result = JSON.parse(result);
    if (result && result.err >= 0) {
        if (result.err == 4) {
            shake_box("系统异常,请稍后再试");
        } else if (result.err == 0) {
            if (result.result && result.result.length > 0) {
                var html = "<option value='-1'>请选择城市</option>";
                for (var i = 0; i < result.result.length; i++) {
                    if (state && cityid == result.result[i].cityID) {
                        html += "<option selected='selected' value='" + result.result[i].cityID + "'>" + result.result[i].city + "</option>"
                    } else {
                        html += "<option value='" + result.result[i].cityID + "'>" + result.result[i].city + "</option>"
                    }
                }
                $("#city").html(html);
            }
        } else {
            shake_box("暂无数据城市数据");
        }
    } else {
        shake_box("系统异常,请稍后再试");
    }
}

function AreaList(state,area_id,city_id){
    var result = GetAreaByCityId(city_id);
    result = JSON.parse(result);
    if (result && result.err >= 0) {
        if (result.err == 4) {
            shake_box("系统异常,请稍后再试");
        } else if (result.err == 0) {
            if (result.result && result.result.length > 0) {
                var html = "<option value='-1'>请选择区县</option>";
                for (var i = 0; i < result.result.length; i++) {
                    if (state && area_id == result.result[i].areaID) {
                        html += "<option selected='selected' value='" + result.result[i].areaID + "'>" + result.result[i].area + "</option>"
                    } else {
                        html += "<option value='" + result.result[i].areaID + "'>" + result.result[i].area + "</option>"
                    }
                }
                $("#area").html(html);
            }
        } else {
            shake_box("暂无数据区县数据");
        }
    } else {
        shake_box("系统异常,请稍后再试");
    }
}

function GetSchool(area_id){
    var result = GetSchoolByArea(area_id);
    result = JSON.parse(result);
    if (result && result.err >= 0) {
        if (result.err == 4) {
            shake_box("系统异常,请稍后再试");
        } else if (result.err == 0) {
            if (result.result && result.result.length > 0) {
                var html = "";
                for (var i = 0; i < result.result.length; i++) {
                    html += "<li><a href='javascript:;' onclick='SchoolClick(this)' data-id='" + result.result[i].school_id + "'>" + result.result[i].school_name + "</a> </li>"
                }
                $(".school_sel").html("<ol>" + html + "</ol>");
            }
        } else {
            shake_box("暂无数据");
        }
    } else {
        shake_box("系统异常,请稍后再试");
    }
}

function GetGradeBySchool(school_id,school_name){
    $.ajax({
        type: "POST",
        async: false,
        url: "http://" + location.host + "/admin/schoolinfo/GetGradeBySchool",
        data: {
            school_id: school_id,
            school_name:school_name
        },
        success: function (msg) {
            if(msg && msg.err == 0 && msg.result && msg.result.length>0){
                var html = '<option value="-1">请选择年级</option>';
                for(var i=0;i<msg.result.length;i++) {
                    html += "<option value='" + msg.result[i].grade_id + "'>" + msg.result[i].grade_name + "</option>"
                }
                $("#grade").html(html);
            }
        },
        dataType: "json"
    });
}

function GetGradeClasses(school_id,school_name){
    var result;
    $.ajax({
        type: "POST",
        async: false,
        url: "http://" + location.host + "/admin/student/GetGradeClass",
        data: {
            school_id: school_id,
            school_name:school_name
        },
        success: function (msg) {
            result = msg;
        },
        dataType: "json"
    });
    return result;
}

/**
 * 省份下拉框改变事件
 */
$('#province').change(function () {
    var result_city_id = $.trim($("#result_city_id").val());
    var province_id = $(this).val();
    if (province_id && province_id != "-1") {
        CityList(false,result_city_id,province_id);
    }else{
        $('#city').html("<option value='-1'>请选择城市</option>")
        $("#area").html("<option value='-1'>请选择区县</option>");
    }
});

/**
 * 城市下拉框改变事件
 */
$("#city").change(function () {
    var result_area_id = $.trim($("#result_area_id").val());
    var city_id = $(this).val();
    if(city_id && city_id != "-1"){
        AreaList(false,result_area_id,city_id);
    }else{
        $("#area").html("<option value='-1'>请选择区县</option>");
    }
});

/**
 * 区县下拉框改变事件
 */
$("#area").change(function () {
    var area_id = $(this).val();
    if (area_id && area_id != '-1') {
        GetSchool(area_id);
    }
});
function gradechangefun(obj){
    var then  = $(obj);
    var grade_id = $(obj).val();
    var school_id = $("#school_id").val();
    var school_name = $("#school_name").val();
    if(grade_id && grade_id != "-1"){
        $.ajax({
            type: "POST",
            async: false,
            url: "http://" + location.host + "/admin/schoolinfo/QueryClassBySchool",
            data: {
                school_id: school_id,
                school_name:school_name,
                grade_id:grade_id
            },
            success: function (msg) {
                if(msg && msg.err == 0 && msg.result && msg.result.length>0){
                    var html = '<option value="-1">请选择班级</option>';
                    for(var i=0;i<msg.result.length;i++) {
                        html += "<option value='" + msg.result[i].class_num + "'>" + msg.result[i].class_num + "</option>"
                    }
                    then.next('select').html(html);
                }
            },
            dataType: "json"
        });
    }else{
        $(obj).siblings('#class_num').html('<option value="-1">请选择班级</option>')
    }
}

//$("#grade").change(function () {
//    var then  = $(this);
//    var grade_id = $(this).val();
//    var school_id = $("#school_id").val();
//    var school_name = $("#school_name").val();
//    $.ajax({
//        type: "POST",
//        async: false,
//        url: "http://" + location.host + "/admin/schoolinfo/QueryClassBySchool",
//        data: {
//            school_id: school_id,
//            school_name:school_name,
//            grade_id:grade_id
//        },
//        success: function (msg) {
//            if(msg && msg.err == 0 && msg.result && msg.result.length>0){
//                var html = '<option value="-1">请选择班级</option>';
//                for(var i=0;i<msg.result.length;i++) {
//                    html += "<option value='" + msg.result[i].class_num + "'>" + msg.result[i].class_num + "</option>"
//                }
//                then.next('select').html(html);
//            }
//        },
//        dataType: "json"
//    });
//});

$('.save_btn').click(function () {
    var province_id = $('#province option:selected').val();
    if(!province_id || province_id == '-1'){
        shake_box("请选择省份");
        return false;
    }
    var city_id = $("#city option:selected").val();
    if(!city_id || city_id == '-1'){
        shake_box("请选择城市");
        return false;
    }
    var area_id = $("#area option:selected").val();
    if(!area_id || area_id == '-1'){
        shake_box("请选择区县");
        return false;
    }
    var address = $.trim($("#address").val());
    var school_id = $.trim($("#school_id").val());
    var school_name = $.trim($("#school_name").val());
    if(!school_name || school_name == ''){
        shake_box("请选择学校");
        return false;
    }
    var school_type = $("#school_type option:selected").val();
    if(!school_type || school_type == "-1"){
        shake_box("请选择学校类型");
        return false;
    }
    var isok = false;
    var isstate = false;
    var list = [];
    $("#grade_list div").each(function (i, o) {
        var grade_id = $(o).find("#grade").val();
        var class_num = $(o).find("#class_num").val();
        if (grade_id && grade_id != '-1' && class_num && class_num != "-1") {
            for (var j = 0; j < list.length; j++) {
                if (grade_id == list[j].grade_id && class_num == list[j].class_num)
                    isok = true;
            }
            var json = {
                grade_id: grade_id,
                class_num: class_num
            };
            list.push(json);
        } else {
            isstate = true;
        }
    });
    if(isok){
        shake_box("班级数据存在重复数据,请确认");
        return false;
    }

    if(isstate){
        shake_box("班级数据存在不完善,请确认");
        return false;
    }
    $('.loadingBox').show();
    $.ajax({
        type: "POST",
        async: false,
        url: "http://" + location.host + "/admin/schoolinfo/save",
        data: {
            province_id: province_id,
            city_id: city_id,
            area_id: area_id,
            address: address,
            school_id: school_id,
            school_name: school_name,
            school_type: school_type,
            list: JSON.stringify(list)
        },
        success: function (msg) {
            $('.loadingBox').hide();
            if (msg) {
                if (msg.err == 10)
                    shake_box("会话已过期,请重新登录");
                else if (msg.err == 0)
                    capacity("保存成功");
                else
                    shake_box("保存失败,请稍后再试")
            } else {
                shake_box("保存失败,请稍后再试")
            }
        },
        dataType: "json"
    });
});
