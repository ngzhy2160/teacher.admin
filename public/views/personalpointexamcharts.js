// 基于准备好的dom，初始化echarts实例
var myChart = echarts.init(document.getElementById('pointexamchart'));

function setexamOptionScat(examdatearray,ratearray,pointname) {

    //var option = {
    //    color: ['#3398DB'],
    //    tooltip : {
    //        trigger: 'axis',
    //        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
    //            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
    //        }
    //    },
    //    grid: {
    //        left: '3%',
    //        right: '4%',
    //        bottom: '3%',
    //        containLabel: true
    //    },
    //    xAxis : [
    //        {
    //            type : 'category',
    //            data : examdatearray,
    //            axisTick: {
    //                alignWithLabel: true
    //            }
    //        }
    //    ],
    //    yAxis : [
    //        {
    //            type : 'value'
    //        }
    //    ],
    //    series : [
    //        {
    //            name:'准确率(%)',
    //            type:'bar',
    //            barWidth : 18,
    //            data:ratearray
    //        }
    //    ]
    //};
    //option = {
    //    title: {
    //
    //    },
    //    tooltip: {
    //        trigger: 'item',
    //        showContent:true,
    //        position:'top',
    //        formatter : function (params) {
    //            return params.value[2];
    //        },
    //        backgroundColor:'#e2e2e2',
    //        textStyle:{
    //            color:'#333'
    //        }
    //    },
    //    calculable: true,
    //    grid: {
    //        borderWidth: 0,
    //        y: 80,
    //        y2: 60
    //    },
    //    xAxis: [
    //        {
    //            type: 'category',
    //            data: examdatearray,
    //            splitLine: {
    //                show:false
    //            },
    //            axisLabel : {
    //                formatter: '{value}'
    //            }
    //
    //        }
    //    ],
    //    yAxis: [
    //        {
    //            type : 'value',
    //            scale:true,
    //            min:0,
    //            axisLabel : {
    //                formatter: '{value}'
    //            },
    //            splitLine: {
    //                show:false
    //            },
    //            axisTick:{
    //                show:false,
    //            },max:100
    //        }
    //    ],
    //    series: [
    //        {
    //            name: '单项知识掌握情况',
    //            type: 'bar',
    //            itemStyle: {
    //                normal: {
    //                    barBorderRadius: 5, // 统一设置四个角的圆角大小
    //                    barBorderRadius: [5, 5, 0, 0], //（顺时针左上，右上，右下，左下）
    //                    color: function(params) {
    //                        // build a color map as your need.
    //                        var colorList = [
    //                            '#C1232B','#B5C334','#FCCE10','#E87C25','#27727B',
    //                            '#FE8463','#9BCA63','#FAD860','#F3A43B','#60C0DD',
    //                            '#D7504B','#C6E579','#F4E001','#F0805A','#26C0C0'
    //                        ];
    //                        return colorList[params.dataIndex]
    //                    },
    //                    label: {
    //                        show: true,
    //                        position: 'top',
    //                        formatter : function (params) {
    //                            return params.value[1]+'%' ;
    //                        }
    //                    }
    //                }
    //            },
    //            data: ratearray,
    //            markPoint: {
    //                tooltip: {
    //                    trigger: 'item',
    //                    backgroundColor: 'rgba(0,0,0,0)',
    //                    formatter: function(params){
    //                        return '<img src="'
    //                            + params.data.symbol.replace('image://', '')
    //                            + '"/>';
    //                    }
    //                },
    //                data: [
    //                    {xAxis:0, y: 350, name:'Line', symbolSize:20, symbol: 'image://../asset/ico/折线图.png'},
    //                    {xAxis:1, y: 350, name:'Bar', symbolSize:20, symbol: 'image://../asset/ico/柱状图.png'},
    //                    {xAxis:2, y: 350, name:'Scatter', symbolSize:20, symbol: 'image://../asset/ico/散点图.png'},
    //                    {xAxis:3, y: 350, name:'K', symbolSize:20, symbol: 'image://../asset/ico/K线图.png'},
    //                    {xAxis:4, y: 350, name:'Pie', symbolSize:20, symbol: 'image://../asset/ico/饼状图.png'},
    //                    {xAxis:5, y: 350, name:'Radar', symbolSize:20, symbol: 'image://../asset/ico/雷达图.png'},
    //                    {xAxis:6, y: 350, name:'Chord', symbolSize:20, symbol: 'image://../asset/ico/和弦图.png'},
    //                    {xAxis:7, y: 350, name:'Force', symbolSize:20, symbol: 'image://../asset/ico/力导向图.png'},
    //                    {xAxis:8, y: 350, name:'Map', symbolSize:20, symbol: 'image://../asset/ico/地图.png'},
    //                    {xAxis:9, y: 350, name:'Gauge', symbolSize:20, symbol: 'image://../asset/ico/仪表盘.png'},
    //                    {xAxis:10, y: 350, name:'Funnel', symbolSize:20, symbol: 'image://../asset/ico/漏斗图.png'},
    //                ]
    //            }
    //        }
    //    ]
    //};

    option = {
        title : {
            //text: '未来一周气温变化',
            //subtext: '纯属虚构'
        },
        tooltip : {
            trigger: 'axis'
        },
        legend: {
            data:['','']
        },
        toolbox: {

        },
        calculable : true,
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                data : examdatearray,
                splitLine: {
                    show:false
                },
                axisLine:{
                    show:false
                },
                axisTick:{
                    show:false,
                }
            }
        ],
        yAxis : [
            {
                type : 'value',
                axisLabel : {
                    formatter: '{value} %'
                },
                splitLine: {
                    lineStyle: {
                        type: 'dashed',
                        color:'#e5e5e5'
                    }
                },
                axisLine:{
                    show:false
                },
                axisTick:{
                    show:false,
                }
            }
        ],
        series : [
            {
                name:pointname,
                type:'line',
                data:ratearray,
                markPoint : {
                    data : [
                        {type : 'max', name: '最大值(%)'},
                        {type : 'min', name: '最小值(%)'}
                    ]
                },
                markLine : {
                    data : [
                        {type : 'average', name: '平均值(%)'}
                    ]
                },
                itemStyle:{
                    normal:{
                        color:'#f79473'
                    }
                }
            }
        ]
    };

    myChart.setOption(option,true,true,true);
}
function showpointexamChart(pointinfo) {
    var exampointsinfo=pointinfo.pointDateTitleArray;

    var ratearray = [];//百分比
    var examdatearray=[];//日期

    for(var i =exampointsinfo.length-1;i>=0;i--) {
        examdatearray.push(exampointsinfo[i].formatdate);
        //ratearray.push(exampointsinfo[i].rate);
        //ratearray.push([i,exampointsinfo[i].rate,exampointsinfo[i].point_name]);
        ratearray.push(exampointsinfo[i].rate);
    }

    if(examdatearray.length==0){
        examdatearray.push('');
    }

    var pointname=pointinfo.pointname||'';

    var option = setexamOptionScat(examdatearray,ratearray,pointname);
}

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}

$('#pointexamSelect').change(function(){
    /*知识掌握图谱 start */
    var pointexamSelect = $(this).children('option:selected').val();

    if(pointexamSelect == "0" || pointexamSelect == 0){
        pointexamSelect=undefined;
    }
    var userid=$('#learn_state_user_id').val();
    $.ajax({
        type:'post',
        url:'/admin/student/queryexampoints',
        data:{userid:userid,pointexamSelect:pointexamSelect},
        dataType: "json",
        success:function(result){
            if (result != null) {
                showpointexamChart(result);
            } else {
                var option = setOptionScat([ 0, 0,'' ], "100", "60");
                myChart.setOption(option);
            }
        },
        error:function(err){
            console.log('err',err)
        }
    });
});

$(document).ready(function() {
    /*知识掌握图谱 start */
    //var pointexamSelect = $("#pointexamSelect").val();
    //var result;
    //showpointexamChart(result);
    var pointexamSelect= $("#pointexamSelect").val();;
    var userid=$('#learn_state_user_id').val();
    $.ajax({
        type:'post',
        url:'/admin/student/queryexampoints',
        data:{userid:userid,pointexamSelect:pointexamSelect},
        dataType: "json",
        success:function(result){
            if (result != null) {
                showpointexamChart(result);
            } else {
                var option = setOptionScat([ 0, 0,'' ], "100", "60");
                myChart.setOption(option);
            }
        },
        error:function(err){
            console.log('err',err)
        }
    });

});




