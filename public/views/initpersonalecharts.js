// 基于准备好的dom，初始化echarts实例
var myChart1 = echarts.init(document.getElementById('pointchart'));
// 指定图表的配置项和数据
function setOptionScat(datalist21, datalist22, datalist23) {

    var option = {
        title : {
            text: '',
            subtext: ''
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        tooltip : {
            trigger: 'item',
            showContent:true,
            position:'top',
            formatter : function (params) {
                return params.value[2] ;
            },
            backgroundColor:'#e2e2e2',
            textStyle:{
                color:'#333'
            },
            extraCssText: 'background:url(/images/tipback.png)  no-repeat center center;width:97px;line-height:28px;text-align:center'
        },
        xAxis : [
            {
                type : 'value',
                scale:true,
                axisLabel : {
                    formatter: '{value}'
                },
                splitLine: {
                    show:false
                },
                axisLine:{
                    show:false
                },
                axisTick:{
                    show:false,
                }
            }
        ],
        yAxis : [
            {
                type : 'value',
                scale:true,
                min:0,
                axisLabel : {
                    formatter: '{value}%'
                },
                splitLine: {
                    lineStyle: {
                        type: 'dashed',
                        color:'#e5e5e5'
                    }
                },
                axisLine:{
                    show:false
                },
                axisTick:{
                    show:false,
                },max:100
            }
        ],
        series : [
            {
                type:'scatter',
                data:datalist21,

                markPoint : {

                },
                markLine : {
                },
                hoverAnimation:false,
                symbol:'image://../images/greendots.png',
            },
            {
                type:'scatter',
                data:datalist22,
                markPoint : {
                },
                markLine : {
                },
                hoverAnimation:false,

                symbol:'image://../images/reddots.png',
            },
            {
                type:'scatter',
                data:datalist23,
                markPoint : {
                },
                markLine : {
                },
                hoverAnimation:false,
                symbol:'image://../images/greydots.png',
            }
        ]
    };
    myChart1.setOption(option,true,true,true);
}

//基于准备好的dom，初始化echarts实例
var symbolstr='image://../images/greendots.png'
var datalist1=[[0, 0,'二次方程'], [1, 5,'解方程组'], [2, 0,'不等式'], [3, 8,'求和公式'], [4, 10,'绝对值'],[5, 8,'因式分解'], [6, 0,'三角形'], [7,3,'圆形']];
function setOptionScat2(datalist1) {
    var option = {
        title: {
            text: ''
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        toolbox: {
            show:false
        },
        tooltip : {
            trigger: 'item',
            showContent:true,
            position:'top',
            formatter : function (params) {
                return params.value[2] ;
            },
            backgroundColor:'#e2e2e2',
            textStyle:{
                color:'#333'
            },
            extraCssText: 'background:url(../images/tipback.png)  no-repeat center center;width:97px;line-height:28px;text-align:center'
        },
        xAxis: {
            type : 'value',
            scale:true,
            axisLabel : {
                formatter: '{value}'
            },

            axisLine:{
                show:false
            },

            axisTick:{
                show:false
            },

            splitLine:{
                show:false
            }
        },
        yAxis: {
            type : 'value',
            scale:true,
            axisLabel : {
                formatter: '{value}'
            },

            axisLine:{
                show:false
            },

            axisTick:{
                show:false,
            },
            splitLine:{
                lineStyle:{
                    color:'#e5e5e5',
                    type:'dashed'
                }
            },
        },
        series: [
            {
                type:'line',
                data:datalist1,
                symbol:symbolstr,
                symbolSize:[14,13],
                lineStyle: {
                    normal: {
                        color: '#e7e7e7'
                    }
                },
                hoverAnimation:false,

            },

        ]
    };
    myChart2.setOption(option,true,true,true);
}
function showChart1(userStatiInfoList) {
    var timeUnit = "";
    var d = [];
    var len = 0;
    var myCharact = [];
    var charactKey;

    var notStudying = [];//未学
    var haveToLearn = [];//已学
    var notCompleted = [];//不及格
    for(var i = 0 ;i<userStatiInfoList.length;i++){

        if(userStatiInfoList[i].haveStuflag == -1){
            notStudying.push([i,0,userStatiInfoList[i].point_name,userStatiInfoList[i].point_id,userStatiInfoList[i].parent_id]);
        }else if(userStatiInfoList[i].haveStuflag == 1){
            if(userStatiInfoList[i].exercTrueRate != null){
                haveToLearn.push([i,userStatiInfoList[i].exercTrueRate,userStatiInfoList[i].point_name,userStatiInfoList[i].point_id,userStatiInfoList[i].parent_id]);
            }
        }else if(userStatiInfoList[i].haveStuflag == 0){
            if(userStatiInfoList[i].exercTrueRate != null){
                notCompleted.push([i,userStatiInfoList[i].exercTrueRate,userStatiInfoList[i].point_name,userStatiInfoList[i].point_id,userStatiInfoList[i].parent_id]);
            }
        }
    }
    var option = setOptionScat(haveToLearn, notCompleted, notStudying);
}

$('#chapterSelect').change(function(){
    /*知识掌握图谱 start */
    var chapterSelect = $(this).children('option:selected').val();
    var userid= $('#learn_state_user_id').val();
    $.ajax({
        type:'post',
        url:'/admin/student/QueryMaterialPoints',
        data:{userid:userid,chapterSelect:chapterSelect},
        dataType: "json",
        success:function(result){
            if (result != null) {
                showChart1(result);
            } else {
                var option = setOptionScat([ 0, 0,'' ], "100", "60");
                myChart1.setOption(option);
            }
        },
        error:function(err){
        }
    });
});

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}
$(document).ready(function() {
    path='';
    /*知识掌握图谱 start */
    var chapterSelect = $("#chapterSelect").val();

    var userid= $('#learn_state_user_id').val();
    $.ajax({
        type:'post',
        url:'/admin/student/QueryMaterialPoints',
        data:{userid:userid,chapterSelect:chapterSelect},
        dataType: "json",
        success:function(result){
            if (result != null) {
                showChart1(result);
            } else {
                var option = setOptionScat([ 0, 0,'' ], "100", "60");
                myChart1.setOption(option);
            }
        },
        error:function(err){
        }
    });



});