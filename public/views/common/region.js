
var GetCityByProvinceId = function(province_id) {
    var result;
    if (province_id) {
        $.ajax({
            type: "POST",
            async: false,
            url: "http://" + location.host + "/common/region/GetCity",
            data: {
                province_id: province_id
            },
            success: function (msg) {
                result = JSON.stringify(msg);
            },
            dataType: "json"
        });
    } else {
        result = JSON.stringify({err: 1});
    }
    return result;
};

var GetAreaByCityId = function (city_id) {
    var result;
    if (city_id) {
        $.ajax({
            type: "POST",
            async: false,
            url: "http://" + location.host + "/common/region/GetArea",
            data: {
                city_id: city_id
            },
            success: function (msg) {
                result = JSON.stringify(msg);
            },
            dataType: "json"
        });
    } else {
        result = JSON.stringify({err: 1});
    }
    return result;
};