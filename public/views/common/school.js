var GetSchoolByArea = function (area_id) {
    var result;
    if (area_id) {
        $.ajax({
            type: "POST",
            async: false,
            url: "http://" + location.host + "/common/school/GetSchoolByArea",
            data: {
                area_id: area_id
            },
            success: function (msg) {
                result = JSON.stringify(msg);
            },
            dataType: "json"
        });
    } else {
        result = JSON.stringify({err: 1});
    }
    return result;
};