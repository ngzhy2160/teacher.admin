// 基于准备好的dom，初始化echarts实例
var useranxietychart = echarts.init(document.getElementById('anxietychart'));
function  initanxietyEcharts(classarray, anxietyarray,maxstudentsnumber){
    var option = {
        backgroundColor: '#fff',
        color: [
            '#e15f41', '#8fc31f', '#80F1BE','#e04009','#4b7af0','#f04be6'
        ],
        legend: {
            y: 'top',
            data: classarray,
            textStyle: {
                color: '#666',
                fontSize: 16
            }
        },
        grid: {
            left: '0%',
            right: '6%',
            bottom: '3%',
            containLabel: true
        },
        tooltip: {
            padding: 10,
            backgroundColor: '#fffdf7',
            borderColor: '#ccc',
            borderWidth: 1,
            formatter: function (obj) {
                var value = obj.value;
                return '<div style="border-bottom: 1px solid rgba(255,255,255,.3); font-size: 18px;padding-bottom: 7px;margin-bottom: 7px">'
                    + obj.seriesName + ' ' + value[7] + ''
                    + '</div>'
                    +  '焦虑值：' + value[1] + '<br>';
            },
            textStyle: {
                color: '#333'
            }
        },
        xAxis: {
            type: 'value',
            name: '人数',
            nameGap: 16,
            nameTextStyle: {
                color: '#666',
                fontSize: 14
            },
            max: maxstudentsnumber,
            splitLine: {
                show: false
            },
            axisLine: {
                lineStyle: {
                    color: '#333',
                    width:0
                }
            }
        },
        yAxis: {
            type: 'value',
          //  name: '焦虑值',
            nameLocation: 'end',
            nameGap: 20,
            max:50,
            nameTextStyle: {
                color: '#9d9d9d',
                fontSize: 12
            },
            axisLine: {
                lineStyle: {
                    color: '#9d9d9d',
                    width:0
                }
            },
            splitLine: {
                lineStyle: {
                    type: 'dashed',
                    color:'#cccccc'
                }
            }
        },
        visualMap: [
            {
                left: 'right',
                top: '10%',
                dimension: 2,
                min: 0,
                max: 250,
                itemWidth: 0,
                itemHeight: 0,
                calculable: false,
                precision: 0.1,
                textGap: 30,
                textStyle: {
                    color: '#fff'
                },
                inRange: {
                    symbolSize: [10, 80]
                },
                outOfRange: {
                    symbolSize: [10, 80],
                    color: ['rgba(255,255,255,.2)']
                },
                controller: {
                    inRange: {
                        color: ['#c23531']
                    },
                    outOfRange: {
                        color: ['#444']
                    }
                }
            }
        ],
        series: anxietyarray
    };

    useranxietychart.setOption(option,true,true,true);
}
function  initanxietyechartsFun() {
    var gradeid = $("#grade").val();
    if (gradeid && gradeid != '0') {

        var itemStyle = {
            normal: {
               // opacity: 0.8,
               // shadowBlur: 10,
               // shadowOffsetX: 0,
               // shadowOffsetY: 0,
               // shadowColor:'#ccc'
            }
        };

        $.ajax({
            type: "POST",
            url: "/admin/anxiety/getStudentAnxiety",
            data: {
                gradeid: gradeid
            },
            dataType: "json",
            success: function (result) {
                //console.log('userratedata',result.studentAnxietyArray);
                //result = JSON.parse(result);
                if (result && result.studentAnxietyArray && result.studentAnxietyArray.length > 0) {
                    var classarray = result.classnumArray;//班级
                    var maxstudentsnumber=result.maxstudentsnumber;
                    if(maxstudentsnumber <8){
                        maxstudentsnumber=7;
                    }
                    var anxietyarray = [];//焦虑值
                    var anxietylist = result.studentAnxietyArray;

                    var dataArray=[];
                    var dataObject={};
                    if (anxietylist && anxietylist.length <= 0) {
                        //dataObject.name="";
                        //dataObject.type='scatter';
                        //dataObject.itemStyle=itemStyle;
                        //dataObject. data=dataArray;
                        //anxietyarray.push(dataObject);
                    }
                    else {
                        for (var i = 0; i < anxietylist.length; i++) {
                            dataArray=[];
                            dataObject={};
                            var indexanxietyArray=anxietylist[i].anxietyArray;
                            if(indexanxietyArray&&indexanxietyArray.length>0){
                                for(var anietyindex=0;anietyindex<indexanxietyArray.length;anietyindex++){
                                    var anxietyscore=indexanxietyArray[anietyindex].math_anxiety_score;
                                    if (anxietyscore) {
                                        dataArray.push([anietyindex+1,anxietyscore,anxietyscore,0,0,0,50,indexanxietyArray[anietyindex].student_name]);
                                    }
                                    else {
                                        anxietyarray.push([anietyindex+1,0,0,0,0,0,50,'']);
                                    }
                                }
                                dataObject.name=anxietylist[i].classnum+"班";
                                dataObject.type='scatter';
                                dataObject.itemStyle=itemStyle;
                                dataObject. data=dataArray;

                                anxietyarray.push(dataObject)
                            }
                        }
                    }
                    //console.log('classarray',classarray);
                    //console.log('anxietyarray',anxietyarray);
                    initanxietyEcharts(classarray, anxietyarray,maxstudentsnumber);
                }
                else {
                    initanxietyEcharts([''], [0],7);
                }
            }
        });
    }
    else {
        shake_box('请选择年级！');
    }
}

$(document).ready(function() {
    //initanxietyechartsFun([''],[0],7);
});