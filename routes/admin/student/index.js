var router = require('express').Router();

router.get(['/','/index'], function (req,res) {
    res.app.set('view options', {layout: true});
    var teacher_id = req.session.teacherid;
    var page = req.query.page || 1;
    if (req.session.teacherid) {
        helper.co(function* () {
            var result = [];
            var teacher_result = yield function (callback) {
                entity.student_query.QueryTeacher(teacher_id, callback);
            };
            if (teacher_result && teacher_result.length > 0) {
                var classes_result = yield function (callback) {
                    entity.student_query.QueryClassesByTeacherId(teacher_id, callback);
                };
                if (classes_result && classes_result.length > 0) {
                    for (var i = 0; i < classes_result.length; i++) {
                        var student_result = yield function (callback) {
                            entity.student_query.QueryClassStudent(teacher_id,teacher_result[0].school_id, teacher_result[0].school_name, classes_result[i].grade_id, classes_result[i].class_num, page, callback);
                        };
                        if (student_result && student_result.err == 0 && student_result.result) {
                            classes_result[i].link = '/admin/student/page?grade_id='+classes_result[i].grade_id+'&class_num='+classes_result[i].class_num;
                            classes_result[i].page_list = student_result.result;
                        }
                    }
                }
                result = classes_result;
            }

            return result;
        }).then(function (value) {
            res.render("admin/student/index", {result: value});
        }).catch(function (err) {
            console.error(err);
            res.render("admin/student/index", {result: []});
        });
    } else {
        res.redirect('/admin/login');
    }
});

/**
 * 分页
 */
router.get('/page', function (req,res) {
    res.app.set('view options', {layout: false});
    var page = req.query.page || 1;
    var grade_id = req.query.grade_id;
    var teacher_id = req.session.teacherid;
    //var teacher_id='37d68497523e6f1e09844d4a746a16cf';
    var class_num = req.query.class_num;

    helper.co(function* () {
        var result = {};
        var teacher_result = yield function (callback) {
            entity.student_query.QueryTeacher(teacher_id, callback);
        };
        if(teacher_result && teacher_result.length>0){
            var classes_result = yield function (callback) {
                entity.student_query.QueryClassesByTeacherIdGradeId(teacher_id,grade_id,class_num,callback);
            };
            if(classes_result && classes_result.length>0){
                var student_result = yield function (callback) {
                    entity.student_query.QueryClassStudent(teacher_id,teacher_result[0].school_id, teacher_result[0].school_name, grade_id, class_num, page, callback);
                };
                if (student_result && student_result.err == 0 && student_result.result) {
                    classes_result[0].link = '/admin/student/page?grade_id='+ grade_id+'&class_num='+ class_num;
                    classes_result[0].page_list = student_result.result;
                }

                return classes_result;
            }else{
                return result;
            }
        }else{
            return result;
        }

    }).then(function (value) {
        res.render('admin/student/page',{result:value});
    }).catch(function (err) {
        console.error(err);
        res.render("admin/student/page",{result:{}})
    })
});

/**
 * 删除学生信息
 */
router.post("/del_student", function (req,res) {
    var grade_id = req.body.grade_id;
    var list = JSON.parse(req.body.list);
    var teacher_id = req.session.teacherid;
    if (req.session.teacherid) {
        if (list && list.length > 0) {
            helper.co(function* () {
                var result = yield function (callback) {
                    entity.student_operation.DelStudent(teacher_id,grade_id, list, callback);
                };
                if (result) return result;
                else return {err: 1};
            }).then(function (value) {
                res.end(JSON.stringify(value));
            }).catch(function (err) {
                console.error('sadas',err);
                res.end(JSON.stringify({err: 1}));
            })
        } else {
            res.end(JSON.stringify({err: 1}));
        }
    } else {
        res.end(JSON.stringify({err: 10}));
    }
});

/**
 * 添加学生信息
 */
router.post('/save_student', function (req,res) {
    var student_id = req.body.student_id;
    var user_id = req.body.user_id;
    var teacher_id = req.session.teacherid;
    var grade_id = req.body.grade_id;
    var class_num = req.body.class_num
    if (req.session.teacherid ) {
        if (student_id && user_id) {
            helper.co(function* () {
                var result = yield function (callback) {
                    entity.student_operation.SaveStudent(teacher_id, grade_id, class_num, student_id, user_id, callback);
                };
                if (result) return result;
                else return {err: 1};

            }).then(function (value) {
                res.end(JSON.stringify(value));
            }).catch(function (err) {
                console.error(err);
                res.end(JSON.stringify({err: 1}));
            })
        } else {
            res.end(JSON.stringify({err: 1}));
        }
    } else {
        res.end(JSON.stringify({err: 10}));
    }
});

/**
 * 获取已删除的学生信息
 */
router.post('/GetStudent', function (req,res) {
    var grade_id = req.body.grade_id;
    var class_num = req.body.class_num;
    var teacher_id = req.session.teacherid;
    if (req.session.teacherid) {
        if (grade_id && class_num) {
            helper.co(function* () {
                var result = yield function (callback) {
                    entity.student_operation.QueryTeacherStudent(teacher_id, grade_id, class_num, callback);
                };
                if (result) return {err: 0, result: result};
                else return {err: 1};

            }).then(function (value) {
                res.end(JSON.stringify(value));
            }).catch(function (err) {
                console.error(err);
                res.end(JSON.stringify({err: 1}));
            })
        } else {
            res.end(JSON.stringify({err: 1}));
        }
    } else {
        res.end(JSON.stringify({err: 10}));
    }
});

/**
 * 学生学习信息
 */
router.get('/learn_state',function(req,res){
    res.app.set('view options', {layout: false});
    var userid=req.query.user_id;

    var result={};
    helper.co(function* () {
        var points = yield function (callback) {
            entity.student_query.QueryExamPointsByUserid(userid, callback);
        };
        points = JSON.parse(points);
        result.chappointArray=points.result;

        //根据userid查询用户信息和用户扩展信息
        var userinfo = yield function (callback) {
            entity.student_query.QueryUserInfoByUserId(userid, callback)
        };
        if (userinfo && userinfo.err == 0 && userinfo.result && userinfo.result.length > 0) {

            var subject_id = userinfo.result[0].subject_id;
            var mater_id = userinfo.result[0].mater_id;
            var grade_id = userinfo.result[0].grade_id;

            var chap = yield function(callback){
                entity.student_query.QueryChap(subject_id,mater_id,grade_id,callback);
            };
            chap = JSON.parse(chap);
            if(chap && chap.err == 0 && chap.result && chap.result.length>0)
                result.chap = chap.result;
        }
        result.user_id = userid;
        result.userinfo = userinfo;
        return result;
    }).then(function (value) {
        res.render('admin/student/learn_state',{practiceinfo: value});
    }).catch(function (err) {
        console.error(err);
    });
});

router.post('/queryexampoints',function(req,res) {
    helper.co(function* () {
        var pointinfo={};

        var selectpointid = req.body.pointexamSelect;
        var userid = req.body.userid;

        var pointDateTitle = yield function (callback) {
            entity.student_query.QueryExamPointsDateTitle(userid, selectpointid, callback);
        }
        pointDateTitle = JSON.parse(pointDateTitle);

        var pointDateTitleArray = pointDateTitle.result;


        var examPointsItems = yield function (callback) {
            entity.student_query.QueryExamPointsItems(userid, selectpointid, callback);
        }
        examPointsItems = JSON.parse(examPointsItems);

        var examPointsItemsArray = examPointsItems.result;

        for (var index = 0; index < pointDateTitleArray.length; index++) {
            var createtime = pointDateTitleArray[index].createtime;
            var result = examPointsItemsArray.filter(function (item) {
                return item.createtime == createtime;
            })
            if (result && result[0]) {
                var answer=0;
                var correctanswer=0;
                var totalquestioncount=result.length;
                var truequestioncount=0;
                for(var qindex=0;qindex<result.length;qindex++){
                    answer=result[qindex].answer;
                    correctanswer=result[qindex].correct_answer;
                    if(answer>0 &&answer == correctanswer){
                        truequestioncount+=1;
                    }
                }

                pointDateTitleArray[index].rate=Math.ceil((truequestioncount/totalquestioncount)*100);
            }
            else {
                pointDateTitleArray[index].rate=0;
            }
        }
        pointinfo.pointDateTitleArray=pointDateTitleArray;
        pointinfo.pointname='';
        if(examPointsItemsArray && examPointsItemsArray.length>0){
            pointinfo.pointname=examPointsItemsArray[0].point_name;
        }

        return  pointinfo;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
    })
});

/**
 * 获取教材对应年级下所有知识点
 */
router.post('/QueryMaterialPoints',function(req,res) {
    helper.co(function* () {
        var userid = req.body.userid;
        var selectchapterid = req.body.chapterSelect;
        var examuserinfo = {};
        //获取会员扩展信息表
        var examuser = yield function (callback) {
            entity.student_query.QueryExamStudent(userid, callback);
        }
        if (examuser) {
            examuserinfo = JSON.parse(examuser);
        }

        if(examuserinfo.result&&examuserinfo.result.length>0) {
            var mater_id = examuserinfo.result[0].mater_id;
            var subject_id = examuserinfo.result[0].subject_id;
            var grade_id = examuserinfo.result[0].grade_id;
            var pointsresult = yield function (callback) {
                entity.student_query.QueryMaterialPoints(subject_id, mater_id, grade_id, callback);
            }
            pointsresult = JSON.parse(pointsresult);
            var allpoints = pointsresult.result;
        }
        else{

            return;
        }

        var parentidarray;


        if (selectchapterid && selectchapterid != "0") {
            parentidarray = allpoints.filter(function (item) {
                return (item.parent_id == "0" && item.point_id == selectchapterid )
            });
        }
        else {
            parentidarray = allpoints.filter(function (item) {
                return item.parent_id == "0"
            });
        }
        pointsarray = [];

        for (var index = 0; index < parentidarray.length; index++) {
            var pointid = parentidarray[index].point_id;
            var childpoints = getchildpoints(allpoints, pointid);
            var points = getpoints(childpoints);
        }
        var pointidarraystring;
        if (pointidarray && pointidarray.length > 0) {
            pointidarraystring = "'" + pointidarray.join("','") + "'"
        }
        var pointrate = yield function (callback) {
            entity.student_query.QueryExamanalysisQuescountByUser(userid, pointidarraystring, callback);
        }
        pointrate = JSON.parse(pointrate);

        var pointratearray = pointrate.result;

        for (var anysisindex = 0; anysisindex < pointsarray.length; anysisindex++) {
            var vpointid = pointsarray[anysisindex].point_id;
            var result = pointratearray.filter(function (item) {
                return item.klpoint_id == vpointid;
            })
            pointsarray[anysisindex].haveStuflag = 0;

            if (result && result[0] && result[0].true_ques_count) {
                pointsarray[anysisindex].exercTrueRate = (result[0].true_ques_count / result[0].dup_answ_ques_count) * 100;
            }
            else {
                pointsarray[anysisindex].exercTrueRate = 0;
            }
        }
        return  pointsarray;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
    })
});

/**
 * 获取年级班级信息
 */
router.post('/GetGradeClass', function (req,res) {
    var school_id = req.body.school_id;
    var school_name = req.body.school_name;

    helper.co(function* () {
        var result = yield function (callback) {
            entity.common_school.QueryGradeBySchool(school_name, school_id, callback);
        };
        if (result && result.length > 0) {
            for (var i = 0; i < result.length; i++) {
                var classes_result = yield function (callback) {
                    entity.common_school.QueryClassBySchool(result[i].grade_id, school_id, school_name, callback);
                };
                if(classes_result && classes_result.length>0){
                    result[i].classes_list = classes_result;
                }
            }

            return result;
        } else return {err: 1};
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 4}));
    })
});

module.exports = router;

function getchildpoints(result, pid) {
    var  rtn = [];
    for (var i in result) {
        if (result[i].parent_id == pid) {
            result[i].children = getchildpoints(result, result[i].point_id);
            if(result[i].children.length==0){
            }
            rtn.push(result[i]);
        }
    }
    return rtn;
}

var pointsarray = [];
var pointidarray=[];
function getpoints(results) {
    for (var index in results) {
        if (results[index].children && results[index].children.length > 0) {
            for (var cindex in results[index].children) {
                if (results[index].children[cindex] && results[index].children[cindex].children.length == 0) {
                    pointsarray.push(results[index].children[cindex]);
                    pointidarray.push(results[index].children[cindex].point_id);
                }
                else{
                    getpoints(results[index].children[cindex].children);
                }
            }
        }
        else {
            pointsarray.push(results[index]);
            pointidarray.push(results[index].point_id);
        }
    }
    return pointsarray;
}