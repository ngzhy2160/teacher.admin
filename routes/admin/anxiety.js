var express = require('express');
var router = express.Router();

/* GET register page. */
router.get('/', function(req, res, next) {
    //取消模板继承
    res.app.set('view options', {layout: false});

    var result = {};
    if (req.session.teacherid) {
        helper.co(function* () {
            var teacherid = req.session.teacherid;

            var classesinforesult = yield function (callback) {
                entity.classes_classDao.selectbyteacherid(teacherid, callback);
            }
            classesinforesult = JSON.parse(classesinforesult);
            if (classesinforesult && classesinforesult.result && classesinforesult.result.length > 0) {
                result.gradelistinfo= classesinforesult.result;
            }

            //console.log('result',result);
            return result;
        }).then(function (value) {
            res.render('admin/anxiety', {result: value});

        }).catch(function (err) {
            console.error(err);
        })
    }
    else {
        res.redirect('/admin/login');
    }
});

/**
 * 获取教师年级下班级
 */
router.post('/getClasses',function(req,res) {
    req.session.materinfo=null;

    if (req.session.teacherid == null || req.session.teacherid == undefined) {
        res.redirect('/admin/login');
        res.end();
        return;
    }
    var result={};
    helper.co(function* () {
        var teacherid = req.session.teacherid;
        //年级id
        var gradeid = req.body.gradeid;

        //获取班级
        var classesinfo = yield function (callback) {
            entity.classes_classDao.selectgradeclassesbyteacherid(teacherid,gradeid, callback);
        }
        classesinfo = JSON.parse(classesinfo);
        //console.log('classesinfo',classesinfo)
        var classnumArray=[];
        if(classesinfo && classesinfo.result && classesinfo.result.length>0){
            for(var classnumindex=0;classnumindex<classesinfo.result.length;classnumindex++){
                classnumArray.push(classesinfo.result[classnumindex].class_num);
            }
        }
        result.classnumArray=classnumArray;
        //console.log('classnumArray',classnumArray);
        return result;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
    })
});


/**
 * 获取学生焦虑信息
 */
router.post('/getStudentAnxiety',function(req,res) {
    req.session.materinfo=null;

    var result={};
    helper.co(function* () {
        var teacherid = req.session.teacherid;
        //年级id
        var gradeid = req.body.gradeid;
        //var classnum = req.body.classnum;
        var schoolid;

        //学校
        var teacherinfo = yield function (callback) {
            entity.personalcenter_personalDao.getteacherinfoById(teacherid, callback);
        }
        teacherinfo = JSON.parse(teacherinfo);

        if (teacherinfo && teacherinfo.result && teacherinfo.result.length > 0) {
            schoolid = teacherinfo.result[0].school_id;
        }

        //教材 学科
        var mater_id;
        var subject_id;
        var grade_id = gradeid;

        var materInfo = yield function (callback) {
            entity.consoledesk_consoledeskDao.QueryMaterInfo(schoolid, gradeid, callback);
        }
        materInfo = JSON.parse(materInfo);
        if (materInfo && materInfo.result && materInfo.result.length > 0) {
            mater_id = materInfo.result[0].mater_id;
            subject_id = materInfo.result[0].subject_id;
        }

        //获取班级
        var classesinfo = yield function (callback) {
            entity.classes_classDao.selectgradeclassesbyteacherid(teacherid,gradeid, callback);
        }
        classesinfo = JSON.parse(classesinfo);
        //console.log('classesinfo',classesinfo)
        var classnumArray=[];
        var studentAnxietyArray=[];
        var maxstudentsnumber=0;

        if(classesinfo && classesinfo.result && classesinfo.result.length>0){
            var indexclassnum='';
            var anxietyResult={};
            for(var classnumindex=0;classnumindex<classesinfo.result.length;classnumindex++){
                indexclassnum=classesinfo.result[classnumindex].class_num;
                classnumArray.push(indexclassnum+"班");

                anxietyResult={};
                anxietyResult.classnum=indexclassnum;
                var studentAnxietyResult = yield function (callback) {
                    entity.anxiety_anxietyDao.getStudentAnxiety(schoolid,grade_id, mater_id,subject_id, indexclassnum, callback);
                }
                studentAnxietyResult = JSON.parse(studentAnxietyResult);

                if (studentAnxietyResult && studentAnxietyResult.result && studentAnxietyResult.result.length > 0) {
                    anxietyResult.anxietyArray=studentAnxietyResult.result;
                    if(maxstudentsnumber<studentAnxietyResult.result.length){
                        maxstudentsnumber=studentAnxietyResult.result.length;
                    }
                }
                else{
                    anxietyResult.anxietyArray={};
                }
                studentAnxietyArray.push(anxietyResult);
            }
        }
        result.classnumArray=classnumArray;
        result.studentAnxietyArray=studentAnxietyArray;
        result.maxstudentsnumber=maxstudentsnumber;
        //console.log('studentAnxietyResult.result',studentAnxietyArray)

        //console.log('anxietyresult',result);
        return result;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
    })
});

module.exports = router;