var express = require('express');
var router = express.Router();

/* GET register page. */
router.get('/', function(req, res, next) {
    //模板继承
    res.app.set('view options', {layout: true});

    var result = {};
    if (req.session.teacherid) {
        var teacherid=req.session.teacherid;
        helper.co(function* () {
            var teacherinforesult = yield function (callback) {
                entity.personalcenter_personalDao.getteacherinfoById(teacherid,callback);
            }
            teacherinforesult = JSON.parse(teacherinforesult);
            if (teacherinforesult && teacherinforesult.result && teacherinforesult.result.length > 0) {
                result.teacherinfo= teacherinforesult.result[0];
                var now = new Date(result.teacherinfo.birthday);

                var year = now.getFullYear();
                var month = (now.getMonth() + 1 > 9) ? (now.getMonth() + 1) : ('0' + (now.getMonth() + 1));
                var date = (now.getDate() > 9) ? now.getDate() : ('0' + now.getDate());
                result.year = year;
                result.month = month;
                result.day = date;
            }
            //console.log('result',result);
            return result;
        }).then(function (value) {
            res.render('admin/setting', {result: value});
        }).catch(function (err) {
            console.error(err);
        })
    }
    else {
        res.redirect('/admin/login');
    }
});

/**
 * 保存教师常规设置基本信息
 */
router.post('/savegeneralsetting',function(req,res,next){
    if(req.session.teacherid == null){
        res.redirect('admin/login');
        return;
    }
    var teacherid=req.session.teacherid;
    var account = req.body.account;
    var fullname=req.body.fullname;
    var nickname = req.body.nickname;
    var sex=req.body.sex ||'1';
    var birthdayyear=req.body.birthdayyear;
    var birthdaymonth=req.body.birthdaymonth;
    var birthdayday=req.body.birthdayday;

    var generalinfo={};
    generalinfo.teacherid=teacherid;
    generalinfo.account=account;
    generalinfo.fullname=fullname;
    generalinfo.nickname=nickname;
    generalinfo.sex=sex;
    generalinfo.birthday=birthdayyear+"-"+birthdaymonth+"-"+birthdayday;

    helper.co(function* () {
        //常规设置
        var baseteacherinfo = yield function (callback) {
            entity.personalcenter_personalDao.savegeneralsetting(generalinfo, callback);
        };
        baseteacherinfo = JSON.parse(baseteacherinfo);
        //console.log('registerresult', baseteacherinfo);
        var result = {};
        result.err=0;
        if(baseteacherinfo&&baseteacherinfo.result&&baseteacherinfo.result.affectedRows ==1){
            result.err = 1;
        }
        return result;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 0}));
    })
});

module.exports = router;