var express = require('express');
var router = express.Router();

/* GET register page. */
router.get('/', function(req, res, next) {
    //取消模板继承
    res.app.set('view options', {layout: true})
    //res.render('admin/securitysetting', {result: {}});
    var result = {};
    if (req.session.teacherid) {
        var teacherid=req.session.teacherid;
        helper.co(function* () {
            var teacherinforesult = yield function (callback) {
                entity.personalcenter_personalDao.getteacherinfoById(teacherid,callback);
            }
            teacherinforesult = JSON.parse(teacherinforesult);
            if (teacherinforesult && teacherinforesult.result && teacherinforesult.result.length > 0) {
                result.teacherinfo= teacherinforesult.result[0];
            }
            //console.log('result',result);
            return result;
        }).then(function (value) {
            res.render('admin/securitysetting', {result: value});
        }).catch(function (err) {
            console.error(err);
        })
    }
    else {
        res.redirect('/admin/login');
    }
});


/**
 * 判断原始密码输入是否正确
 */
router.post('/checkpassword',function(req,res,next){
    var password = req.body.password;
    //md5
    password = helper.md5(password);

    helper.co(function* () {
        var result = {};
        if(req.session.teacherid == null){
            result.num = 0;
        }
        else {
            var teacherid = req.session.teacherid;
            var pwdinforesult = yield function (callback) {
                entity.personalcenter_personalDao.checkpassword(teacherid,password, callback);
            };
            pwdinforesult = JSON.parse(pwdinforesult);
            //console.log('result', result.result[0]);
            if (pwdinforesult && pwdinforesult.result && pwdinforesult.result.length > 0) {
                result.num = 1;//密码正确
            }
            else {
                result.num = 0;
            }
        }
        return result;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 0}));
    })
});

/**
 * 判断手机号是否已绑定
 */
router.post('/checkMobileIsBind',function(req,res,next){
    var phonenumber = req.body.phonenumber;

    helper.co(function* () {
        var result = {};
        if(req.session.teacherid == null){
            result.num = 0;
        }
        else {
            var teacherid = req.session.teacherid;
            var teacherinfo = yield function (callback) {
                entity.personalcenter_personalDao.checkBindMobile(teacherid,phonenumber, callback);
            };
            teacherinfo = JSON.parse(teacherinfo);
            if (teacherinfo && teacherinfo.result && teacherinfo.result.length > 0) {
                result.num = 1;//手机号已绑定
            }
            else {
                result.num = 0;
            }
        }
        return result;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 0}));
    })
});

/**
 * 判断验证码输入是否正确
 */
router.post('/checkverifcode',function(req,res,next){
    var verifcode = req.body.verifcode ||'';

    helper.co(function* () {
        var result = {};
        if(req.session.validcode.toLowerCase() == verifcode.toLowerCase() && verifcode!= undefined){
            result.num = 1;//验证码正确
        }
        else{
            result.num = 0;
        }
        return result;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 0}));
    })
});

/**
 * 保存安全隐私
 */
router.post('/savesecuritysetting',function(req,res,next){
    if(req.session.teacherid == null){
        res.redirect('admin/login');
        return;
    }
    var teacherid=req.session.teacherid;
    var newpassword = req.body.newpassword;
    var phonenumber=req.body.phonenumber;
    //md5
    newpassword = helper.md5(newpassword);

    helper.co(function* () {
        //安全隐私
        var teacherinfo = yield function (callback) {
            entity.personalcenter_personalDao.updatesecuritysetting(teacherid,newpassword,phonenumber, callback);
        };
        teacherinfo = JSON.parse(teacherinfo);
        //console.log('registerresult', teacherinfo);
        var result = {};
        result.err=0;
        if(teacherinfo&&teacherinfo.result&&teacherinfo.result.affectedRows ==1){
            result.err = 1;
        }
        return result;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 0}));
    })
});

/**
 * 发送验证码
 */
router.post('/sendsms',function(req,res,next){
    var code = get_id_code();
    var tel = req.body.tel;
    var message = "【数加教育】您的验证码为：" + code + "，请及时完成密码的修改,如非本人操作，请忽略。";
    var type = 2;
    helper.co(function *() {
        var insert_log = yield function (callback) {
            entity.smslog_smslogDao.insertsmscode(code,type,tel, callback);
        }
        insert_log=JSON.parse(insert_log);
        if (insert_log&&insert_log.result && insert_log.result.affectedRows == 1) {
            var sms_log = yield function (callback) {
                entity.smslog_smslogDao.SendSMS(tel, message, callback);
            };
            if (sms_log == '0') {
                return {err: 0};
            } else {
                return {err: 1};
            }
        } else {
            return {err: 1};
        }

    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 1}));
    });
})

/**
 * 绑定手机号码
 */
router.post('/BindTel', function (req,res) {
    if(req.session.teacherid == null){
        res.redirect('admin/login');
        return;
    }

    var mobile = req.body.tel;
    var code = req.body.code;
    var userid = req.session.teacherid;

    if (userid) {
        helper.co(function* () {
            //查询该手机号码是否绑定过
            var teacherid = userid;
            var verificationMobile = yield function (callback) {
                entity.personalcenter_personalDao.checkBindMobile(mobile, callback);
            };
            verificationMobile = JSON.parse(verificationMobile);
            //console.log('verificationMobile', verificationMobile)

            if (verificationMobile && verificationMobile.result && verificationMobile.result.length > 0) {
                return {err: 7, message: '该手机号码已绑定账号,不能重复绑定'}
            } else {
                /**
                 * 查询验证码
                 * @type {string}
                 */
                var find_Code = yield function (callback) {
                    entity.smslog_smslogDao.selectsmscode(2,mobile, callback);
                }
                find_Code = JSON.parse(find_Code);
                //console.log('find_Code', find_Code)
                if (find_Code && find_Code.result && find_Code.result.length > 0) {
                    var code_id = find_Code.result[0].id;
                    if (find_Code.result[0].code != code)
                        return {err: 6, status: 1, message: '验证码输入错误'};

                    if (new Date().getTime() - find_Code.result[0].create_time > 1000 * 60 * 30)
                        return {err: 6, status: 2, message: "验证码已过期"};

                    /**
                     * 修改手机号码
                     */

                    var update_pwd = yield function (callback) {
                        entity.personalcenter_personalDao.updateMobile(teacherid, mobile, callback);
                    };
                    update_pwd = JSON.parse(update_pwd);

                    if (update_pwd && update_pwd.result && update_pwd.result.affectedRows > 0) {
                        var update_code = yield function (callback) {
                            entity.smslog_smslogDao.updateIsuseStatus(code_id, callback);
                        }
                        update_code = JSON.parse(update_code);

                        if (update_code && update_code.result && update_code.result.affectedRows > 0) {
                            return {err: 0};
                        } else {
                            return {err: 0};
                        }
                    } else {
                        return {err: 1};
                    }
                } else {
                    return {err: 6, message: "找不到验证码"};
                }
            }
        }).then(function (value) {
            res.end(JSON.stringify(value));
        }).catch(function (err) {
            console.error(err);
            res.end(JSON.stringify({err: 1}));
        });
    } else {
        res.end(JSON.stringify({err: 5}));//未登录或session已过期
    }
});

/**
 * 解除绑定手机号码
 */
router.post('/unBindTel', function (req,res) {
    var mobile = req.body.tel;
    var code = req.body.code;
    var userid = req.session.teacherid;

    if(userid){
        helper.co(function* () {
            //查询该手机号码是否绑定过
            var teacherid = userid;
            var verificationMobile = yield function (callback) {
                entity.personalcenter_personalDao.checkBindMobile(mobile, callback);
            };
            verificationMobile = JSON.parse(verificationMobile);

            if (verificationMobile && verificationMobile.result && verificationMobile.result.length > 0) {
                /**
                 * 查询验证码
                 * @type {string}
                 */
                var find_Code = yield function (callback) {
                    entity.smslog_smslogDao.selectsmscode(2,mobile, callback);
                }
                find_Code = JSON.parse(find_Code);
                if (find_Code && find_Code.result && find_Code.result.length > 0) {
                    var code_id = find_Code.result[0].id;
                    if (find_Code.result[0].code != code)
                        return {err: 6, status: 1, message: '验证码输入错误'};

                    if (new Date().getTime() - find_Code.result[0].create_time > 1000 * 60 * 30)
                        return {err: 6, status: 2, message: "验证码已过期"};

                    /**
                     * 修改手机号码
                     */
                    var update_pwd = yield function (callback) {
                        entity.personalcenter_personalDao.updateMobile(teacherid, '', callback);
                    };
                    update_pwd = JSON.parse(update_pwd);

                    if (update_pwd && update_pwd.result && update_pwd.result.affectedRows > 0) {
                        var update_code = yield function (callback) {
                            entity.smslog_smslogDao.updateIsuseStatus(code_id, callback);
                        }
                        update_code = JSON.parse(update_code);

                        if (update_code && update_code.result && update_code.result.affectedRows > 0) {
                            return {err: 0};
                        } else {
                            return {err: 0};
                        }
                    } else {
                        return {err: 1};
                    }
                } else {
                    return {err: 6, message: "找不到验证码"};
                }
            }else{
                return {err:7,message:'该手机号码未绑定过账号'};
            }

        }).then(function (value) {
            res.end(JSON.stringify(value));
        }).catch(function (err) {
            console.error(err);
            res.end(JSON.stringify({err: 1}));
        });
    }else{
        res.end(JSON.stringify({err:5}));//未登录或session已过期
    }
});

/**
 * 随机数
 * @returns {string}
 */
function get_id_code(){
    var text_range = ['0','1','2','3','4','5','6','7','8','9'];
    var id_code = '';
    for(var i = 0; i < 4 ; i++){
        var _i = Math.floor(Math.random()*text_range.length) ;
        id_code += text_range[_i];
    }
    return id_code;
}

module.exports = router;