var express = require('express');
var router = express.Router();

/* GET register page. */
router.get('/', function(req, res, next) {
    res.app.set('view options', {layout: true});
    if (req.session.teacherid) {
        var teacher_id = req.session.teacherid;
        helper.co(function* () {
            var result = {};
            /**
             * 根据教师ID查询教师信息
             */
            var teacheruserinfo_result = yield function (callback) {
                entity.schoolinfo_query.QueryTeacherUserInfoByTeacherId(teacher_id, callback);
            };
            if (teacheruserinfo_result && teacheruserinfo_result.length > 0) result.teacherinfo = teacheruserinfo_result[0];
            else result.teacherinfo = {};

            /**
             * 根据教师ID查询班级信息
             */
            var teacherclass_result = yield function (callback) {
                entity.schoolinfo_query.QueryTeacherClassByTeachId(teacher_id, callback);
            };
            if (teacherclass_result && teacherclass_result.length > 0) result.teacherclasses = JSON.stringify(teacherclass_result);
            else result.teacherclasses = JSON.stringify([]);

            /**
             * 查询省份
             */
            var province_result = yield function (callback) {
                entity.common_region.QueryProvince(callback);
            };
            if (province_result && province_result.length > 0) result.province = province_result;
            else result.province = [];

            /**
             * 查询学校类型
             */
            var school_type = yield function (callback) {
                entity.common_school.QuerySchoolType(callback);
            };

            if (school_type && school_type.length > 0) result.school_type = school_type;
            else result.school_type = [];

            return result;
        }).then(function (value) {
            res.render('admin/schoolinfo', {result: value});
        }).catch(function (err) {
            console.error(err);
            res.render('admin/schoolinfo', {result: {}});
        });
    } else {
        res.redirect('/admin/login');
    }
});

router.post("/GetGradeBySchool", function (req,res) {
    var school_id = req.body.school_id;
    var school_name = req.body.school_name;
    helper.co(function* () {
        var result = yield function (callback) {
            entity.common_school.QueryGradeBySchool(school_name, school_id, callback);
        };

        if (result && result.length > 0) return {err: 0, result: result};
        else return {err: 1};
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 4}));
    })
});

router.post("/QueryClassBySchool", function (req,res) {
    var school_id = req.body.school_id;
    var school_name = req.body.school_name;
    var grade_id = req.body.grade_id;

    helper.co(function* () {
        var result = yield function (callback) {
            entity.common_school.QueryClassBySchool(grade_id, school_id, school_name, callback);
        };

        if (result && result.length > 0) return {err: 0, result: result};
        else return {err: 1};
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 4}));
    })
});

router.post("/save", function (req,res) {
    var province_id = req.body.province_id;
    var city_id = req.body.city_id;
    var area_id = req.body.area_id;
    var address = req.body.address;
    var school_id = req.body.school_id;
    var school_name = req.body.school_name;
    var school_type = req.body.school_type;
    var list = JSON.parse(req.body.list);
    if (req.session.teacherid) {
        helper.co(function* () {
            var result = yield function (callback) {
                entity.schoolinfo_operation.SaveTeacherInfo(req.session.teacherid, province_id, city_id, area_id, address, school_id, school_name, school_type, list, callback)
            };
            if (result) {
                if (result && result.err == 0)
                    req.session.locked = 0;
                return result;
            } else return {err: 1};
        }).then(function (value) {
            res.end(JSON.stringify(value));
        }).catch(function (err) {
            console.error(err);
            res.end(JSON.stringify({err: 4}));
        })
    } else {
        res.end(JSON.stringify({err: 10}));
    }
});

module.exports = router;