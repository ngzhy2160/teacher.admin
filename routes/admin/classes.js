var express = require('express');
var router = express.Router();

router.get("/",function(req,res,next) {
    res.app.set('view options', {layout: false});

    helper.co(function* () {
        var result = {};

        //获取学校类型
        var school_type_result = yield function (callback) {
            helper.common.QuerySchoolType(callback);
        }

        result.school_type = school_type_result || {};

        return result;
    }).then(function (value) {
        res.render('center/setting/learninginformation', {result: value});
    }).catch(function (err) {
        console.error(err);
        res.render('center/setting/learninginformation', {result: {}});
    });


    res.render('admin/classes', {result: {}});
})
module.exports = router;
