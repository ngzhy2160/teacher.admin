var express = require('express');
var router = express.Router();

/* 注册 */
router.get('/', function(req, res, next) {
    //取消模板继承
    res.app.set('view options', {layout: false});
    var result={};
    helper.co(function* (){
        var schooltyperesult=yield function(callback){
            entity.schooltype_schooltypeDao.getschooltype(callback);
        }
        schooltyperesult=JSON.parse(schooltyperesult);
        if(schooltyperesult &&schooltyperesult.result&& schooltyperesult.result.length>0){
            result.schooltype=schooltyperesult.result;
        }
        return result;
    }).then(function(value){
        res.render('admin/register', {result: value});
    }).catch(function(err){
        console.error(err);
    })
});

/**
 * 注册
 */
router.post('/register',function(req,res,next){
    var account = req.body.account;
    var fullname=req.body.fullname;
    var password = req.body.password;
    var schoolname=req.body.schoolname;
    var schooltype=req.body.schooltype;
    //md5
    password = helper.md5(password);

    helper.co(function* () {
        //注册
        var registerresult = yield function (callback) {
            entity.register_registerDao.register(account,fullname,password,schoolname,schooltype, callback);
        };
        registerresult = JSON.parse(registerresult);
        //console.log('registerresult', registerresult);
        var result = {};
        result.err=0;
        if(registerresult&&registerresult.result&&registerresult.result.affectedRows ==1){
            result.err = 1;
        }
        return result;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 0}));
    })
});


/**
 * 判断账号是否已注册
 */
router.post('/checkaccountexist',function(req,res,next){
    var account = req.body.account;

    helper.co(function* () {
        var result = {};

        var accountresult = yield function (callback) {
            entity.register_registerDao.checkaccountExist(account, callback);
        };
        accountresult = JSON.parse(accountresult);
        //console.log('accountresult', accountresult.result[0]);
        if (accountresult && accountresult.result && accountresult.result.length > 0) {
            result.num = 1;//账号已存在
        }
        else{
            result.num = 0;
        }
        //console.log('result', result);

        return result;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 0}));
    })
});

module.exports = router;