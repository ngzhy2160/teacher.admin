var express = require('express');
var router = express.Router();

/* GET register page. */
router.get('/', function(req, res, next) {
    //取消模板继承
    res.app.set('view options', {layout: false})
    res.render('admin/login', {result: {}});
});

/**
 * 登录
 */
router.post('/login',function(req,res,next) {
    var account = req.body.account || '';
    var password = req.body.password || '';
    //md5
    password = helper.md5(password);

    helper.co(function* () {
        //登录
        var loginresult = yield function (callback) {
            entity.register_registerDao.login(account, password, callback);
        };
        loginresult = JSON.parse(loginresult);

        var result = {};
        if (loginresult && loginresult.result && loginresult.result.length > 0) {
            req.session.teacherid = loginresult.result[0].teacher_id;
            req.session.nickname = loginresult.result[0].nickname;
            req.session.teacherinfo = loginresult.result[0];
            req.session.locked = loginresult.result[0].islocked || 0;
            result.err = 0;//账号或密码正确
        }
        else {
            result.err = 2;//账号或密码错误
        }
        return result;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 1}));
    })
});

router.post('/sendemail', function (req,res,next) {
    var email = req.body.email||'';
    var code = get_id_code();
    var title = "修改密码";
    var message = "【数加教育】您的验证码为：" + code + "，请及时完成密码的修改,如非本人操作，请忽略。<a href='http://" + req.headers.host + "/admin/resetpwd?email=" + email + "'>修改密码</a>";

    var guid = helper.GUID();
    var type = 1;
    var create_time = new Date().getTime();

    helper.co(function *() {

        var teacherinfo = yield function (callback) {
            entity.personalcenter_personalDao.selectbyaccount(email, callback);
        };
        teacherinfo = JSON.parse(teacherinfo);

        if (teacherinfo && teacherinfo.result && teacherinfo.result.length <= 0) {
            return {err: 2};
        } else {

            var insert_log = yield function (callback) {
                entity.smslog_smslogDao.insertsmscode(code,type,email, callback);
            }
            insert_log=JSON.parse(insert_log);

            if (insert_log &&insert_log.result && insert_log.result.affectedRows == 1) {
                var sms_log = yield function (callback) {
                    entity.smslog_smslogDao.SendEmail(email, message,title, callback);
                };
                //sms_log=JSON.parse(sms_log)

                if (sms_log && sms_log.err == 0)
                    return {err: 0};
                else
                    return {err: 1};
            } else
                return {err: 1};
        }
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 1}));
    });
});

/**
 * POST 修改密码  邮件方式
 */
router.post('/update_pwd_email', function (req,res,next) {
    var email = req.body.email;
    var pwd1 = req.body.pwd1;
    var pwd2 = req.body.pwd2;
    var code = req.body.code;

    //邮箱验证
    var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!reg.test(email))
        res.end(JSON.stringify({err: 2, status: 201, message: 'Email无效'}));

    //密码验证
    var pwd1_result = CheckPwd(pwd1);
    pwd1_result = JSON.parse(pwd1_result);
    if(pwd1_result.err !=0)
        res.end(JSON.stringify({err:3,message:pwd1_result.message}));

    var pwd2_result = CheckPwd(pwd2);
    pwd2_result = JSON.parse(pwd2_result);
    if(pwd2_result.err !=0)
        res.end(JSON.stringify({err:4,message:pwd2_result.message}));

    if (pwd1 != pwd2)
        res.end(JSON.stringify({err: 4, message: '两次输入的密码不一致'}));

    helper.co(function* () {
        /**
         * 查询验证码
         * @type {string}
         */
        var Find_Code_Sql = "select * from log_update_password where type = 1 and Isuse = 0 and account='" + email + "' order by create_time desc limit 1";
        var Find_Code = yield function (callback) {
            helper.mysql.query('', Find_Code_Sql, callback);
        };
        if (Find_Code && Find_Code.length > 0) {
            var code_id = Find_Code[0].id;
            if (Find_Code[0].code != code)
                return {err: 6, status: 1, message: '验证码输入错误'};

            if (new Date().getTime() - Find_Code[0].create_time > 1000 * 60 * 30)
                return {err: 6, status: 2, message: "验证码已过期"};

            /**
             * 修改密码
             */
            var update_pwd_sql = "update tb_sysmgr_user set password = '" + helper.md5(pwd1) + "' where email='" + email + "'";
            var update_pwd = yield function (callback) {
                helper.mysql.query('', update_pwd_sql, callback);
            };
            if (update_pwd && update_pwd.affectedRows > 0) {
                var update_code_sql = "update log_update_password set Isuse = 1 where id='" + code_id + "'";
                var update_code = yield function (callback) {
                    helper.mysql.query('', update_code_sql, callback);
                };
                if (update_code && update_code.affectedRows > 0) {
                    return {err: 0};
                } else {
                    return {err: 0};
                }
            } else {
                return {err: 1};
            }
        } else {
            return {err: 6, message: "找不到验证码"};
        }
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 1}));
    });
});

router.post('/loginout', function (req,res) {
    req.session.teacherid = null;
    req.session.nickname = null;
    req.session.teacherinfo = null;
    res.end(JSON.stringify({err: 0}));
});

/**
 * 随机数
 * @returns {string}
 */
function get_id_code(){
    var text_range = ['0','1','2','3','4','5','6','7','8','9'];
    var id_code = '';
    for(var i = 0; i < 4 ; i++){
        var _i = Math.floor(Math.random()*text_range.length) ;
        id_code += text_range[_i];
    }
    return id_code;
}

module.exports = router;