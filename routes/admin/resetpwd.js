var express = require('express');
var router = express.Router();

/* GET register page. */
router.get('/', function(req, res, next) {
    //取消模板继承
    res.app.set('view options', {layout: false});

    res.render('admin/resetpwd', {result: null});
});

router.post('/updatepassword',function(req,res,next){
    var email = req.body.email||'';
    var newpassword=req.body.password;
    //md5
    newpassword = helper.md5(newpassword);

    helper.co(function *() {
        var updateresult = yield function (callback) {
            entity.personalcenter_personalDao.updatepasswordbyaccount(newpassword,email, callback);
        }
        updateresult=JSON.parse(updateresult);
        console.log('updateresult',updateresult);

        if (updateresult&&updateresult.result && updateresult.result.affectedRows == 1) {
            return {err: 1};
        } else {
            return {err: 0};
        }

    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 1}));
    });
})

/**
 * 判断验证码输入是否正确
 */
router.post('/checkverifcode',function(req,res,next){
    var verifcode = req.body.verifcode ||'';
    var email=req.body.email;

    helper.co(function* () {
        var result = {};
        var find_Code = yield function (callback) {
            entity.smslog_smslogDao.selectsmscode(1,email, callback);
        }
        find_Code = JSON.parse(find_Code);
        console.log('find_Code', find_Code)
        if (find_Code && find_Code.result && find_Code.result.length > 0) {
            //var code_id = find_Code.result[0].id;
            if (find_Code.result[0].code != verifcode)
                return {err: 6, status: 1, message: '验证码输入错误'};

            if (new Date().getTime() - find_Code.result[0].create_time > 1000 * 60 * 30)
                return {err: 6, status: 2, message: "验证码已过期"};
        }
        return result;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 0}));
    })
});
module.exports = router;