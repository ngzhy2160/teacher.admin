var express = require('express');
var router = express.Router();

/* 控制台 */
router.get('/', function(req, res, next) {
    //模板继承
    res.app.set('view options', {layout: true});

    var result = {};
    if (req.session.teacherid) {
        var teacherid = req.session.teacherid;
        helper.co(function* () {
            var classesinforesult = yield function (callback) {
                entity.classes_classDao.selectbyteacherid(teacherid, callback);
            }
            classesinforesult = JSON.parse(classesinforesult);
            if (classesinforesult && classesinforesult.result && classesinforesult.result.length > 0) {
                result.gradelistinfo= classesinforesult.result;
            }
            return result;
        }).then(function (value) {
            res.render('admin/consoledesk', {result: value});
        }).catch(function (err) {
            console.error(err);
        })
    }
    else {
        res.redirect('/admin/login');
    }
});


/**
 * 获取教材对应年级下所有知识点
 */
router.post('/getPoints',function(req,res) {
    req.session.materinfo=null;

    if (req.session.teacherid == null || req.session.teacherid == undefined) {
        res.redirect('/admin/login');
        res.end();
        return;
    }
    pointsarray=[];
    var result={};
    helper.co(function* () {
        req.session.schoolid=null;
        var teacherid = req.session.teacherid;
        //年级id
        var gradeid = req.body.gradeid;

        var teacherinfo = yield function (callback) {
            entity.personalcenter_personalDao.getteacherinfoById(teacherid, callback);
        }
        teacherinfo = JSON.parse(teacherinfo);
        var schoolid;
        if (teacherinfo && teacherinfo.result && teacherinfo.result.length > 0) {
            schoolid=teacherinfo.result[0].school_id;
            req.session.schoolid=schoolid;
        }
        else{
            return;
        }

        var mater_id;
        var subject_id;
        var grade_id = gradeid;


        var materInfo = yield function (callback) {
            entity.consoledesk_consoledeskDao.QueryMaterInfo(schoolid,gradeid, callback);
        }
        materInfo = JSON.parse(materInfo);
        if (materInfo && materInfo.result && materInfo.result.length > 0) {
            req.session.materinfo=materInfo.result[0];
            mater_id=materInfo.result[0].mater_id;
            subject_id=materInfo.result[0].subject_id;
        }
        else {
            return;
        }

        var charplist = yield function (callback) {
            entity.consoledesk_consoledeskDao.QueryChap(subject_id, mater_id, grade_id, callback);
        }
        charplist = JSON.parse(charplist);
        var parentidarray=[];
        if (charplist && charplist.result && charplist.result.length > 0) {
            for(var index=0;index<charplist.result.length;index++){
                parentidarray.push({ point_id: charplist.result[index].point_id });
            }
        }

        var pointsresult = yield function (callback) {
            entity.consoledesk_consoledeskDao.QueryMaterialPoints(subject_id, mater_id, grade_id, callback);
        }
        pointsresult = JSON.parse(pointsresult);
        var allpoints = pointsresult.result;

        for (var index = 0; index < parentidarray.length; index++) {
            var pointid = parentidarray[index].point_id;
            var childpoints = getchildpoints(allpoints, pointid);
            var points = getpoints(childpoints);
        }

        result.pointsarray=pointsarray;
        //获取班级
        var classesinfo = yield function (callback) {
            entity.classes_classDao.selectgradeclassesbyteacherid(teacherid,gradeid, callback);
        }
        classesinfo = JSON.parse(classesinfo);
        //console.log('classesinfo',classesinfo)
        var classnumArray=[];
        if(classesinfo && classesinfo.result && classesinfo.result.length>0){
            for(var classnumindex=0;classnumindex<classesinfo.result.length;classnumindex++){
                classnumArray.push(classesinfo.result[classnumindex].class_num);
            }
        }
        result.classnumArray=classnumArray;
        //console.log('classnumArray',classnumArray)

        return result;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
    })
});

/**
 * 获取班级知识点准确率
 */
router.post('/getClassesPointrate',function(req,res) {
    if (req.session.teacherid == null || req.session.teacherid == undefined) {
        res.redirect('/admin/login');
        res.end();
        return;
    }

    var result={};
    helper.co(function* () {
        var teacherid = req.session.teacherid;
        //年级id
        var gradeid = req.body.gradeid;
        var pointid = req.body.pointid;

        //学校
        var schoolid;
        if(req.session.schoolid){
            schoolid=req.session.schoolid;
        }
        else{
            var teacherinfo = yield function (callback) {
                entity.personalcenter_personalDao.getteacherinfoById(teacherid, callback);
            }
            teacherinfo = JSON.parse(teacherinfo);

            if (teacherinfo && teacherinfo.result && teacherinfo.result.length > 0) {
                schoolid = teacherinfo.result[0].school_id;
            }
        }

        //教材 学科
        var mater_id;
        var subject_id;
        var grade_id = gradeid;

        var basematerinfo;
        if(req.session.materinfo){
            basematerinfo=req.session.materinfo;
            mater_id = basematerinfo.mater_id;
            subject_id = basematerinfo.subject_id;
        }
        else {
            var materInfo = yield function (callback) {
                entity.consoledesk_consoledeskDao.QueryMaterInfo(schoolid, gradeid, callback);
            }
            materInfo = JSON.parse(materInfo);
            if (materInfo && materInfo.result && materInfo.result.length > 0) {
                req.session.materinfo = materInfo.result[0];
                mater_id = materInfo.result[0].mater_id;
                subject_id = materInfo.result[0].subject_id;
            }
        }


        //班级
        var classesinfo = yield function (callback) {
            entity.classes_classDao.selectgradeclassesbyteacherid(teacherid,gradeid, callback);
        }
        classesinfo = JSON.parse(classesinfo);

        var classesArray=[];
        var userIdArray=[];
        var classRate=[];
        var userRate=[];

        if (classesinfo && classesinfo.result && classesinfo.result.length > 0) {
            var classnum;
            for(var index=0;index<classesinfo.result.length;index++){

                //获取指定班级下所有学生
                classnum= classesinfo.result[index].class_num;

                classesArray.push({classnum:classnum});
                var studentslist = yield function (callback) {
                    entity.consoledesk_consoledeskDao.GetAllStudents(schoolid,grade_id, mater_id,subject_id, classnum, callback);
                }
                studentslist = JSON.parse(studentslist);

                if (studentslist && studentslist.result && studentslist.result.length > 0) {

                    var userid;
                    var useridstring = '';
                    for (var yindex = 0; yindex < studentslist.result.length; yindex++) {
                        userid = studentslist.result[yindex].user_id;
                        userIdArray.push(userid);

                        if (yindex == studentslist.result.length - 1) {
                            useridstring += "'" + userid + "'"
                        }
                        else {
                            useridstring += "'" + userid + "',"
                        }
                    }

                    var classrateinfo = yield function (callback) {
                        entity.consoledesk_consoledeskDao.GetClassRate(useridstring, pointid, callback);
                    }
                    classrateinfo = JSON.parse(classrateinfo);
                    var pointrate = 0;
                    if (classrateinfo && classrateinfo.result && classrateinfo.result.length > 0) {
                        pointrate = classrateinfo.result[0].pointrate
                    }
                    classRate.push({classnum: classnum, rate: pointrate})
                }
                else{
                    classRate.push({classnum: classnum, rate: 0})
                }
            }
        }
        result.classRate=classRate;

        return result;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
    })
});

/**
 * 获取用户知识点准确率
 */
router.post('/getUserPointrate',function(req,res) {
    if (req.session.teacherid == null || req.session.teacherid == undefined) {
        res.redirect('/admin/login');
        res.end();
        return;
    }

    var result={};
    helper.co(function* () {
        var teacherid = req.session.teacherid;
        //年级id
        var gradeid = req.body.gradeid;
        var pointid = req.body.pointid;
        var classnum=req.body.classnum;

        //学校
        var schoolid;
        if(req.session.schoolid){
            schoolid=req.session.schoolid;
        }
        else{
            var teacherinfo = yield function (callback) {
                entity.personalcenter_personalDao.getteacherinfoById(teacherid, callback);
            }
            teacherinfo = JSON.parse(teacherinfo);

            if (teacherinfo && teacherinfo.result && teacherinfo.result.length > 0) {
                schoolid = teacherinfo.result[0].school_id;
            }
        }

        //教材 学科
        var mater_id;
        var subject_id;
        var grade_id = gradeid;

        var basematerinfo;
        if(req.session.materinfo){
            basematerinfo=req.session.materinfo;
            mater_id = basematerinfo.mater_id;
            subject_id = basematerinfo.subject_id;
        }
        else {
            var materInfo = yield function (callback) {
                entity.consoledesk_consoledeskDao.QueryMaterInfo(schoolid, gradeid, callback);
            }
            materInfo = JSON.parse(materInfo);
            if (materInfo && materInfo.result && materInfo.result.length > 0) {
                req.session.materinfo = materInfo.result[0];
                mater_id = materInfo.result[0].mater_id;
                subject_id = materInfo.result[0].subject_id;
            }
        }

        var userRate=[];

        //获取指定班级下所有学生
        var studentslist = yield function (callback) {
            entity.consoledesk_consoledeskDao.GetAllStudents(schoolid,grade_id, mater_id,subject_id, classnum, callback);
        }
        studentslist = JSON.parse(studentslist);

        if (studentslist && studentslist.result && studentslist.result.length > 0) {

            var userid;
            for(var index=0;index<studentslist.result.length;index++){
                userid=studentslist.result[index].user_id;

                var userrateinfo = yield function (callback) {
                    entity.consoledesk_consoledeskDao.GetUserRate(userid,pointid, callback);
                }
                userrateinfo = JSON.parse(userrateinfo);
                if (userrateinfo && userrateinfo.result && userrateinfo.result.length > 0) {
                    userRate.push({classnum:classnum,rate:userrateinfo.result[0].pointrate,studentname:userrateinfo.result[0].student_name})
                }
            }
        }

        result.userRate=userRate;
        return result;
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
    })
});


/**
 * 获取知识点
 * @param result
 * @param pid
 * @returns {Array}
 */
function getchildpoints(result, pid) {
    var  rtn = [];
    for (var i in result) {
        if (result[i].parent_id == pid) {
            result[i].children = getchildpoints(result, result[i].point_id);
            if(result[i].children.length==0){
            }
            rtn.push(result[i]);
        }
    }
    return rtn;
}

var pointsarray = [];
var pointidarray=[];
function getpoints(results) {
    for (var index in results) {
        if (results[index].children && results[index].children.length > 0) {
            for (var cindex in results[index].children) {
                if (results[index].children[cindex] && results[index].children[cindex].children.length == 0) {
                    pointsarray.push(results[index].children[cindex]);
                    pointidarray.push(results[index].children[cindex].point_id);
                }
                else{
                    getpoints(results[index].children[cindex].children);
                }
            }
        }
        else {
            pointsarray.push(results[index]);
            pointidarray.push(results[index].point_id);
        }
    }
    return pointsarray;
}


module.exports = router;