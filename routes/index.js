var express = require('express');
var router = express.Router();

/* GET home page. */
router.get(['/','index'], function(req, res, next) {
  res.app.set('view options',{layout:false});
  res.render('admin/login', { title: 'Express' });
});

module.exports = router;
