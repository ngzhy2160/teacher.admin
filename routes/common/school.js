var router = require("express").Router();

router.post("/GetSchoolByArea", function (req,res) {
    var area_id = req.body.area_id;
    helper.co(function* () {
        var result = yield function (callback) {
            entity.common_school.QuerySchoolByAreaId(area_id, callback);
        };

        if (result && result.length > 0)
            return {err: 0, result: result};
        else
            return {err: 1};
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 4}));
    })
});

module.exports = router;