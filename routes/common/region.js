var router = require("express").Router();

/**
 * 获取省份
 */
router.post("/GetProvince", function (req,res) {
    helper.co(function* () {
        var province_result = yield function (callback) {
            entity.common_region.QueryProvince(callback);
        };
        if (province_result && province_result.length > 0)
            return {err: 0, result: province_result};
        else
            return {err: 1};
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 4}))
    })
});

/**
 * 根据省份ID查询城市信息
 */
router.post("/GetCity",function(req,res) {
    var province_id = req.body.province_id;//省份ID
    helper.co(function*() {
        var city_result = yield function (callback) {
            entity.common_region.QueryCityByProvinceId(province_id, callback);
        };

        if (city_result && city_result.length > 0)
            return {err: 0, result: city_result};
        else
            return {err: 1};
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 4}));
    })
});

/**
 * 根据城市ID查询区县
 */
router.post("/GetArea", function (req,res) {
    var city_id = req.body.city_id;//城市ID
    helper.co(function* () {
        var area_result = yield function (callback) {
            entity.common_region.QueryAreaByCityId(city_id, callback);
        };

        if (area_result && area_result.length > 0)
            return {err: 0, result: area_result};
        else
            return {err: 1};
    }).then(function (value) {
        res.end(JSON.stringify(value));
    }).catch(function (err) {
        console.error(err);
        res.end(JSON.stringify({err: 4}));
    })
});

module.exports = router;