var express = require('express');
var router = express.Router();
router.get('/', function(req, res, next) {
    res.app.set('view options', {layout: false});
    var s = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var code = '';
    for (var i = 0; i < 4; i++) {
        code += s.substr(parseInt(Math.random() * 36), 1);
    }
    var ccap = require('ccap')(
        {
            width: 200,
            offset:52,
            generate: function () {
                this.width;
                this.height;
                return code;
            },
        }
    )
    var ary = ccap.get();
    req.session.validcode = ary[0];
    //console.log('validcode',ary[0])
    var buf = ary[1];
    res.end(buf)
});
module.exports = router;