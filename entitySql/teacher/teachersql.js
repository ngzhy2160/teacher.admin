/**
 * Created by zy on 16-9-18.
 */
var teacher={
    //注册
    register:"insert into tb_teacher_user (`teacher_id`, `account`, `password`, `fullname`, `school_name`, `school_type`, `create_time`,`islocked`) values('%s','%s','%s','%s','%s','%s','%s',%s)",
    //登录
    selectbyaccountandpwd:"select  teacher_id,account,nickname,fullname,sex,islocked,birthday,mobile,school_name,province_id,city_id,area_id,address,school_type from tb_teacher_user where account='%s' and `password`='%s';",
    //获取教师班级信息
    getteacherclasses:"SELECT tb_teacher_classes.grade_id,tb_exam_grade.grade_name,class_num FROM tb_teacher_classes LEFT JOIN tb_exam_grade on tb_teacher_classes.grade_id=tb_exam_grade.grade_id where teacher_id='%s';",
    selectbyid:"select * from tb_teacher_user where teacher_id='%s'",
    checkaccount:"select teacher_id,account from tb_teacher_user where account='%s'",
    selectall:"select * from tb_teacher_user",
    //更新常规设置
    updategeneralsetting:"UPDATE  `tb_teacher_user` SET `account`='%s',`nickname`='%s',`fullname`='%s',`sex`='%s',`birthday`='%s',`update_time`='%s' where `teacher_id`='%s'",
    //判断原始密码输入是否正确
    checkpassword:"select teacher_id,account from tb_teacher_user where  teacher_id='%s' and `password`='%s';",
    //判断手机号是否已绑定
    checkmobile:"SELECT  teacher_id,account,fullname FROM tb_teacher_user  where  mobile='%s';",
    selectbyaccount:"SELECT  teacher_id,account,fullname FROM tb_teacher_user  where  account='%s';",
    //更新安全隐私
    updatesecuritysetting:"UPDATE  `tb_teacher_user` SET `password`='%s',`mobile`='%s',`update_time`='%s' where `teacher_id`='%s'",
    updatepasswordbyaccount:"UPDATE  `tb_teacher_user` SET `password`='%s',`update_time`='%s' where `account`='%s'",
    updateMobile:"UPDATE  `tb_teacher_user` SET `mobile`='%s',`update_time`='%s' where `teacher_id`='%s'"

};
module.exports=teacher;
