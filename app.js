var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
//var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var partials = require('./express-partials');
var session = require("express-session");
var app = express();
app.set('views', path.join(__dirname, 'views'));
app.engine('.html', require('ejs').renderFile);
app.set('view engine', 'html');


app.use(session({
  secret: 'teacher_admin_secret',
  resave: false,
  path:'/',
  saveUninitialized: true,
  cookie: {
    maxAge: 1000 * 60 * 60 * 1  //过期时间设置(单位毫秒)
  }
}));
//加载log
var log = require('./lib/log.js');
log.onload(app,path.join(__dirname,'../'));
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(partials());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req,res,next) {
  if(req.session.locked != 1 && (req.url == '/admin/consoledesk' || req.url == '/admin/student')){
    res.redirect('/admin/setting');
  }else{
    if(req.url == '/' || req.url == '/admin/login') {
      var cookie = req.headers.cookie;
      if (cookie) {
        var username,password;
        var cookieArr = cookie.split(";");
        for (var i = 0, l = cookieArr.length; i < l; i++) {
          if (cookieArr[i].indexOf("=") !== -1) {
            var cookieArrEqual = cookieArr[i].split("=");
            if (cookieArrEqual[0].indexOf('teacher_account') !== -1) {
              username = cookieArrEqual[1];
            }
            if (cookieArrEqual[0].indexOf('teacher_password') !== -1) {
              password = cookieArrEqual[1];
            }
          }
        }
        if (username && password) {
          password = helper.md5(decode64(decodeURIComponent(password)));
          //登录
          entity.register_registerDao.login(username, password, function (err,doc) {
            if(err) next();
            if(doc){
              doc = JSON.parse(doc);
              if (doc && doc.result && doc.result.length > 0) {
                req.session.teacherid = doc.result[0].teacher_id;
                req.session.nickname = doc.result[0].nickname;
                req.session.teacherinfo = doc.result[0];
                req.session.locked = doc.result[0].islocked || 0;
                res.redirect('/admin/consoledesk');
              } else {
                next();
              }
            }
          });
        } else {
          next();
        }
      } else {
        next();
      }
    } else {
      res.clearCookie("teacher_account");
      res.clearCookie("teacher_password");
      res.clearCookie("teacher_remember");
      next();
    }
  }
});

//加载路由
var routers = require('./lib/router.js');
routers.onload(app);
var modules = require('./lib/module.js');
modules.onload(app);

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  //next(err);
  res.redirect('/404');
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;

/**
 * 加密
 * @param input
 * @returns {string}
 */
var keyStr = "ABCDEFGHIJKLMNOP" + "QRSTUVWXYZabcdef" + "ghijklmnopqrstuv" + "wxyz0123456789+/" + "=";
/**
 * 解密
 * @param input
 * @returns {string}
 */
function decode64(input) {
  var output = "";
  var chr1, chr2, chr3 = "";
  var enc1, enc2, enc3, enc4 = "";
  var i = 0;
  // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
  var base64test = /[^A-Za-z0-9\+\/\=]/g;
  if (base64test.exec(input)) {
    console.error("There were invalid base64 characters in the input text.\n" +
        "Valid base64 characters are A-Z, a-z, 0-9, '+', '/', and '='\n" +
        "Expect errors in decoding.");
  }
  try {
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
  } catch (Exception) {
    console.log("Exception", Exception)
  }
  do {
    enc1 = keyStr.indexOf(input.charAt(i++));
    enc2 = keyStr.indexOf(input.charAt(i++));
    enc3 = keyStr.indexOf(input.charAt(i++));
    enc4 = keyStr.indexOf(input.charAt(i++));
    chr1 = (enc1 << 2) | (enc2 >> 4);
    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
    chr3 = ((enc3 & 3) << 6) | enc4;
    output = output + String.fromCharCode(chr1);
    if (enc3 != 64) {
      output = output + String.fromCharCode(chr2);
    }
    if (enc4 != 64) {
      output = output + String.fromCharCode(chr3);
    }
    chr1 = chr2 = chr3 = "";
    enc1 = enc2 = enc3 = enc4 = "";
  } while (i < input.length);
  return output;
}