var helper = {
    items: []
};

helper.addItem = function (name,item) {
    this.items.push({name:name,item:item});
    this[name] = this.items[this.items.length -1].item;
};

/**
 * 增加配置文件
 */
var config = require("./../../config.json");
helper.config = config;

//co
helper.co = require("co");
/**
 * 配置md5
 */
helper.md5 = require('md5');


/**
 * 配置随机数
 */
helper.util=require('util');

helper.moment = require('moment');

/**
 * 配置全局变量
 */
global.helper = helper;

/**
 * 链接mysql数据库
 * @type {{}}
 */
helper.mysql = {};
var mysql_config = require('./../lib/db.js');

helper.GUID = function () {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + S4() + S4() + S4() + S4() + S4() + S4());
};


